package com.arduino.activite;

import android.app.NotificationManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.arduino.dialog.AddServerDialog;
import com.arduino.dialog.ChangeModeDialog;

import com.arduino.R;

/**
 * This class is the first activity execute for this app with this activity we can access to all configuration
 */
public class MainActivity extends FragmentActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.optionmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            //Select the server item into the menu
            case R.id.menu_serveur:
                //Open dialog for edit the server uri
                openDialogServeur();
                return true;
            //Select the sensor item into the menu
            case R.id.menu_capteur:
                //Open activity for select sensor we can get data
                openSensorsActivity();
                return true;
            //Select the mode item into the menu
            case R.id.menu_mode:
                //Open dialog for edit the communication mode
                openDialogMode();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * This method open a dialog for edit the server uri
     */
    protected void openDialogServeur() {
        DialogFragment dialog = new AddServerDialog((App)getApplication());
        dialog.show(getSupportFragmentManager(), "AddServerDialog");
    }

    /**
     * This method open a dialog for edit the communication mode
     */
    protected void openDialogMode() {
        DialogFragment dialog = new ChangeModeDialog((App)getApplication());
        dialog.show(getSupportFragmentManager(), "ChangeModeDialog");
    }

    /**
     * This method open an activity for select sensor we can get data
     */
    protected void openSensorsActivity(){
        Intent i = new Intent(this,SensorsActivity.class);
        startActivity(i);
    }
}