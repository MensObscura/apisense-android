package com.arduino.activite;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.arduino.R;
import com.arduino.sensors.Sensor;

import java.util.List;

/**
 * This class represent the ArrayAdapter for the list of sensor
 */
public class SensorsAdapter extends ArrayAdapter<Sensor> {

    //Context for this adapter
    private final Context context;

    //Sensors array
    private final Sensor[] values;

    private final List<Integer> availableSensor;

    private App app;

    public SensorsAdapter(Context context, Sensor[] values,List<Integer> availableSensor,App app) {
        super(context, R.layout.row_sensors, values);
        this.context = context;
        this.values = values;
        this.availableSensor=availableSensor;
        this.app=app;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        //Get the layout
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        //Get the view of a row
        View rowView = inflater.inflate(R.layout.row_sensors, parent, false);

        //Get the TextView and set the text
        TextView textView = (TextView) rowView.findViewById(R.id.capteurName);
        textView.setText(values[position].getName()+" - "+values[position].getType());

        //Get the checkbox and checked here if the sensor is available
        CheckBox checkbox=(CheckBox)rowView.findViewById(R.id.checkboxCapteur);
        if(availableSensor.contains(values[position].getId())){
            checkbox.setChecked(true);
        }

        //Add a listener on the checkbox
        checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    availableSensor.add(values[position].getId());
                }
                else{
                    availableSensor.remove(new Integer(values[position].getId()));
                }
                app.setSensors();
            }
        });
        return rowView;
    }
}
