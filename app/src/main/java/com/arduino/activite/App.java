package com.arduino.activite;

import android.app.Activity;
import android.app.Application;
import android.bluetooth.BluetoothDevice;

import com.arduino.database.SQLiteHelper;
import com.arduino.database.dao.DaoFactory;
import com.arduino.database.tables.TableFactory;
import com.arduino.link.AsyncApp;
import com.arduino.link.ConnectModule;
import com.arduino.link.SocketArduino;
import com.arduino.module.JSONParser;
import com.arduino.module.Mode;
import com.arduino.module.ModuleArduinoBuilder;
import com.arduino.module.SystemModuleArduino;
import com.arduino.sensors.SensorBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Class represent the Application to transfert data between Android and Arduino
 */
public class App extends Application{

    //synchronize the informations about Modules and Sensors between the server and the telephone.
    private SystemModuleArduino sysModuleArduino;

    //connect arduino between the telephone
    private ConnectModule connectMod;

    //Factory off all database
    private DaoFactory daoFactory;

    //Builder class ModuleArduino
    private ModuleArduinoBuilder moduleBuilder;

    //Builder class Sensors
    private SensorBuilder sensorBuilder;

    //Class execute asynchrone task
    private AsyncApp asyncApp;

    /**
     * Method call at the creation of the app
     */
    public void onCreate(){

        // Récupérer JSON liste Arduino
        final JSONParser jParser = new JSONParser();

        //Initialisation of the SQLiteHelper
        SQLiteHelper helper=new SQLiteHelper(getBaseContext(),SQLiteHelper.NAME_DB,null,SQLiteHelper.VERSION_DB,new TableFactory());

        //Get json represent the config
        JSONObject json=getConfig();

        //Get the server uri
        String serveur=extractServeurUri(json);

        //Get communication mode
        Mode mode=extractMode(json);

        List<Integer> sensors=extractSensor(json);

        //Initialisation of builders
        sensorBuilder=new SensorBuilder();
        moduleBuilder=new ModuleArduinoBuilder();

        //Initialisation of the factory
        daoFactory=new DaoFactory(helper,moduleBuilder,sensorBuilder);

        //Initialisation of the synchronizer informations between Android Application and Arduino
        sysModuleArduino = SystemModuleArduino.getInstance(serveur,sensors,daoFactory,moduleBuilder,sensorBuilder,jParser);

        sysModuleArduino.load();

        //Initialisation  of connection arduino between the telephone
        connectMod=new ConnectModule(sysModuleArduino,mode,getApplicationContext(),this);

        //Execute asynchrone task
        asyncApp=new AsyncApp(sysModuleArduino,connectMod);
        asyncApp.execute();
    }

    /**
     * This method extract the server uri
     * @param json JSon object represent the configuration of the application
     * @return Return the server URI, by default "" is returned
     */
    protected String extractServeurUri(JSONObject json){
        try {
            if(json.has("serveur")) {
               return json.getString("serveur");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * This method extract the communication mode
     * @param json JSon object represent the configuration of the application
     * @return Return the server communication mode, by default PULL is returned
     */
    protected Mode extractMode(JSONObject json){
        try {
            if(json.has("mode")) {
                return Mode.valueOf(json.getString("mode"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return Mode.PULL;
    }

    /**
     * This method extract all sensors activated. The application get from the arduino only this sensors
     * @param json JSon object represent the configuration of the application
     * @return Return id of all sensors activated
     */
    protected List<Integer> extractSensor(JSONObject json){
        ArrayList<Integer> list_sensor=new ArrayList<Integer>();
        try {
            if(json.has("sensor")) {
                JSONArray jsonArray=json.getJSONArray("sensor");
                for(int i=0;i<jsonArray.length();i++){
                    list_sensor.add(jsonArray.getInt(i));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return list_sensor;
    }

    /**
     * This method get the configuration saved into the file "settings.txt"
     * @return Return a JSON Object represent the configuration, default empty json object is returned
     */
    protected JSONObject getConfig(){
        try {
            //Open the file "settings.txt" for read it
            InputStream fIn = openFileInput("settings.txt");
            InputStreamReader isr = new InputStreamReader(fIn);

            //Initialise a buffer to the size of the file
            char[] inputBuffer=new char[fIn.available()];

            //Read the file
            isr.read(inputBuffer);

            //Close the file
            isr.close();

            //return the configuration into a json object
            String str_json= new String(inputBuffer);
            return new JSONObject(str_json);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //Return empty json object
        return new JSONObject();
    }

    /**
     * This method save the configuration into the file "settings.txt"
     */
    protected void setConfig(String url,Mode mode,List<Integer> list_sensor){
        try {
            //Open the file to write into her
            OutputStream fOn = openFileOutput("settings.txt",MODE_PRIVATE);
            OutputStreamWriter osw = new OutputStreamWriter(fOn);

            //Insert configuration into a json object
            JSONObject json=new JSONObject();
            JSONArray jsonSensors=new JSONArray();
            for(int i:list_sensor){
                jsonSensors.put(i);
            }
            json.put("serveur",url);
            json.put("mode",mode.name());
            json.put("sensor",jsonSensors);

            //Write the configuration
            osw.write(json.toString());

            //Close the file
            osw.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Get the server uri
     * @return Return the server uri
     */
    public String getServeur(){
        return sysModuleArduino.getUrl();
    }

    /**
     * Set the server uri and reload data from the server
     * @param url The server uri
     */
    public void setServeur(String url){
        setConfig(url,connectMod.getMode(),sysModuleArduino.getAvailableSensor());
        sysModuleArduino.setURL(url);
        sysModuleArduino.load();
    }

    /**
     * Set all sensors activated into the setting file
     */
    public void setSensors(){
        setConfig(sysModuleArduino.getUrl(),connectMod.getMode(),sysModuleArduino.getAvailableSensor());
    }

    /**
     * Get the communication mode
     * @return the communication mode
     */
    public Mode getMode(){
        return connectMod.getMode();
    }

    /**
     * Get the Data Access Object Factory
     * @return Return the Data Access Object Factory
     */
    public DaoFactory getDaoFactory(){
        return daoFactory;
    }

    /**
     * Set the communication mode between the arduino and android into the setting file
     * @param mode The communication mode
     */
    public void setMode(Mode mode) {
        setConfig(sysModuleArduino.getUrl(),mode,sysModuleArduino.getAvailableSensor());
        connectMod.setMode(mode);
    }

    /**
     * Connect application with an arduino and get data from its sensors
     * @param device Bluetooth device represent the arduino
     */
    public void receiveData(BluetoothDevice device){
        (new Thread(new SocketArduino(device,sysModuleArduino))).start();
    }

    /**
     * Get all sensors activated
     * @return Return all sensors activated
     */
    public List<Integer> getAvailableSensor(){
        return sysModuleArduino.getAvailableSensor();
    }

    /**
     * Get the system of Arduino module
     * @return Return the system of Arduino module
     */
    public SystemModuleArduino getSystemModuleArduino() {
        return sysModuleArduino;
    }
}
