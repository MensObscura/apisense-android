package com.arduino.activite;

import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import com.arduino.R;
import com.arduino.database.dao.DaoException;
import com.arduino.database.dao.SensorsDao;
import com.arduino.sensors.Sensor;

import java.util.ArrayList;
import java.util.List;

/**
 * This activity is used to select sensor we can get data
 */
public class SensorsActivity extends MainActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensors);

        //Get the list view contains the sensors list
        final ListView listview = (ListView) findViewById(R.id.list_sensors);

        //Get the application
        App app= (App)getApplication();

        //Get the sensors list
        SensorsDao sensorsDao=app.getDaoFactory().getSensorDao();
        List<Sensor> sensors=new ArrayList();

        try {
            sensors=sensorsDao.getSensors();
        } catch (DaoException e) {
            e.printStackTrace();
        }

        //Get the Sensors adapter
        final SensorsAdapter adapter = new SensorsAdapter(this, sensors.toArray(new Sensor[0]),app.getAvailableSensor(),app);

        //Set the adapter to the list view
        listview.setAdapter(adapter);
    }
}
