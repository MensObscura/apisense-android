package com.arduino.activite;

import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.arduino.R;

/**
 * This class is the activity execute for accept the connection with an arduino
 */
public class AcceptBluetoothActivity extends MainActivity {

    protected BluetoothDevice device;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accept_bluetooth);

        //Get the bluetooth device
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                device = null;
            } else {
                device = (BluetoothDevice)extras.get("Bluetooth");
            }
        } else {
            device= (BluetoothDevice)  savedInstanceState.get("Bluetooth");
        }
        TextView textview=(TextView)findViewById(R.id.name_bluetooth);
        textview.setText(device.getName()+" a été détecté à proximité");
    }

    /**
     * This method is accept the connection with an arduino when a clicks event on the Yes button is send
     * @param view
     */
    public void acceptBluetooth(View view){
        ((App)getApplication()).receiveData(device);
        Intent intent=new Intent(this,ShowResult.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    /**
     * This method is cancel the connection with an arduino when a clicks event on the No button is send
     * @param view
     */
    public void notAcceptBluetooth(View view){
        Intent intent=new Intent(this,MainActivity.class);
        startActivity(intent);
    }
}
