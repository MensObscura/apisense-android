package com.arduino.activite;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.arduino.R;
import com.arduino.database.dao.DaoException;
import com.arduino.sensors.Sensor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

/**
 * This activity is used show result received by an arduino
 */
public class ShowResult extends MainActivity implements Observer{

    //Map Sensor id with TextView
    private Map<Integer,TextView> mapView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_result);
        mapView=new HashMap<Integer, TextView>();

        //Get the layout
        LinearLayout layout=(LinearLayout)findViewById(R.id.list_resultat);

        App app=(App)getApplication();
        try {

            //Get all sensors
            List<Sensor> list_sensor=app.getDaoFactory().getSensorDao().getSensors();

            //Get available sensor
            List<Integer> available=app.getAvailableSensor();
            TextView t;
            Sensor s;

            //Foreach sensor
            for(int i=0;i<list_sensor.size();i++){

                //If the sensor is available
                if(available.contains(new Integer(list_sensor.get(i).getId()))){

                    //Create new TextView
                    t=new TextView(this);

                    //Get the sensor
                    s=list_sensor.get(i);

                    //Set the name of the sensor to the text view
                    t.setText(s.getName()+" : ");

                    mapView.put(s.getId(),t);

                    //Add TextView into the layout
                    layout.addView(t);
                }
            }
        } catch (DaoException e) {
            e.printStackTrace();
        }
        app.getSystemModuleArduino().addObserver(this);
    }

    @Override
    public void update(Observable observable, final Object data) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //Update data show for a sensor
                Sensor s=(Sensor)data;
                mapView.get(s.getId()).setText(s.getName()+" : "+s.getData());

            }
        });
    }
}
