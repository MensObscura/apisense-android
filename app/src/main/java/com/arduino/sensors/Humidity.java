package com.arduino.sensors;

/**
 * This class represent a humidity sensor
 */
public class Humidity extends Sensor
{
    public Humidity(int id, String name) {
        super(id, name,"Humidity");
    }
}

