package com.arduino.sensors;

/**
 * This class represent a Propane sensor
 */
public class Propane extends Gas
{
    public Propane(int id, String name) {
        super(id, name,"Propane");
    }
}

