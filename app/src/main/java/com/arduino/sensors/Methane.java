package com.arduino.sensors;

/**
 * This class represent a Methane sensor
 */
public class Methane extends Gas
{
    public Methane(int id, String name) {
        super(id, name,"Methane");
    }
}

