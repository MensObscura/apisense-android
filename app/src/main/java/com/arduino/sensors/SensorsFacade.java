package com.arduino.sensors;
import com.arduino.link.SocketArduino;

import java.util.Set;
import java.util.HashSet;

public class SensorsFacade
{
    private SensorBuilder sensorBuilder;
    private SocketArduino socketArduino;

	public SensorsFacade(SensorBuilder sensorBuilder){
		this.sensorBuilder=sensorBuilder;
	}

    public void setSocketArduino(SocketArduino socketArduino){
        this.socketArduino=socketArduino;
    }
	
	public Sensor receiveLightData(int id,String name) {
		Light light=sensorBuilder.createLightSensor(id,name);
        socketArduino.sendRequest(name);
        try {
            double data=parseDataReceived(socketArduino.readData());
            light.setData(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return light;
	}
	
	public Sensor receiveHumidityData(int id,String name) {
        Humidity humidity=sensorBuilder.createHumiditySensor(id,name);
        socketArduino.sendRequest(name);
        try {
            double data=parseDataReceived(socketArduino.readData());
            humidity.setData(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return humidity;
	}
	
	public Sensor receiveSoundData(int id,String name) {
        Sound sound=sensorBuilder.createSoundSensor(id,name);
        socketArduino.sendRequest(name);
        try {
            double data=parseDataReceived(socketArduino.readData());
            sound.setData(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sound;
	}

	public Sensor receiveAlcoholData(int id,String name) {
        Alcohol alcohol=sensorBuilder.createAlcoholSensor(id,name);
        socketArduino.sendRequest(name);
        try {
            double data=parseDataReceived(socketArduino.readData());
            alcohol.setData(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return alcohol;
	}

	public Sensor receiveMethaneData(int id,String name) {
        Methane methane=sensorBuilder.createMethaneSensor(id,name);
        socketArduino.sendRequest(name);
        try {
            double data=parseDataReceived(socketArduino.readData());
            methane.setData(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return methane;
	}

	public Sensor receiveCarbonData(int id,String name) {
        Carbon carbon=sensorBuilder.createCarbonSensor(id,name);
        socketArduino.sendRequest(name);
        try {
            double data=parseDataReceived(socketArduino.readData());
            carbon.setData(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return carbon;
	}

	public Sensor receivePropaneData(int id,String name) {
        Propane propane=sensorBuilder.createPropaneSensor(id,name);
        socketArduino.sendRequest(name);
        try {
            double data=parseDataReceived(socketArduino.readData());
            propane.setData(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return propane;
	}

    protected double parseDataReceived(String data) throws Exception{
        if(data.startsWith("Error")){
            throw new Exception(data.replace("Error : ",""));
        }
        return Double.parseDouble(data);
    }

    public Sensor receivedData(int id,String name,String type){
        if(type.equals("Light")) return receiveLightData(id,name);
        else if(type.equals("Humidity")) return receiveHumidityData(id,name);
        else if(type.equals("Sound")) return receiveSoundData(id,name);
        else if(type.equals("Alcohol")) return receiveAlcoholData(id,name);
        else if(type.equals("Methane")) return receiveMethaneData(id,name);
        else if(type.equals("Carbon")) return receiveCarbonData(id,name);
        else if(type.equals("Propane")) return receivePropaneData(id,name);
        return null;
    }

}

