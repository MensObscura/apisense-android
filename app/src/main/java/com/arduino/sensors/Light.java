package com.arduino.sensors;

/**
 * This class represent a light sensor
 */
public class Light extends Sensor
{
    public Light(int id, String name) {
        super(id, name,"Light");
    }
}

