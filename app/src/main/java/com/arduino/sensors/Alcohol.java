package com.arduino.sensors;


/**
 * This class represent an alcohol sensor
 */
public class Alcohol extends Sensor
{

    public Alcohol(int id, String name) {
        super(id, name,"Alcohol");
    }
}

