package com.arduino.sensors;

/**
 * This class represent a Gas sensor
 */
public class Gas extends Sensor
{


    public Gas(int id, String name) {
        super(id, name,"Gas");
    }

    public Gas(int id, String name,String type) {
        super(id, name,type);
    }
}

