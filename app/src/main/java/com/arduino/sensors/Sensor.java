package com.arduino.sensors;

/**
 * This class represent a sensor
 */
public abstract class Sensor{

    private int id;
    private String name;
    private String type;
    private double data;

    public Sensor(int id,String name,String type) {
        this.id=id;
        this.name = name;
        this.type=type;
    }

    /**
     * Get the sensor name
     * @return Return the sensor name
     */
    public String getName() {
        return this.name;
    }

    /**
     * Get the sensor id
     * @return Return the sensor id
     */
    public int getId() {
        return this.id;
    }

    /**
     * Set data received correspond to this sensor
     * @param data Data received correspond to this sensor
     */
    public void setData(double data){
        this.data=data;
    }

    /**
     * Get the last data received
     * @return Return the last data received
     */
    public double getData(){
        return data;
    }

    /**
     * Get the sensor type
     * @return Return the sensor type
     */
    public String getType(){
        return type;
    }
}
