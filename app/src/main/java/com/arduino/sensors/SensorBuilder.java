package com.arduino.sensors;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * This class represent a builder for sensor
 */
public class SensorBuilder implements Serializable{

    /**
     * Create a sensor from a json object
     * @param json Json object represent a sensor
     * @return Return sensor build
     */
    public Sensor create(JSONObject json){
        int id=-1;
        String name="",type="";

        //Get the sensor id
        try {
            if(json.has("ID")) {
                id = json.getInt("ID");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //Get the sensor name
        try {
            if(json.has("NAME")) {
                name = json.getString("NAME");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //Get the sensor type
        try {
            if(json.has("SENSOR_TYPE")) {
                type = json.getString("SENSOR_TYPE");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //create the sensor
        return create(id,name,type);
    }

    /**
     * Create a sensor from its id, name and type
     * @param id Id of the sensor
     * @param name Name of the sensor
     * @param type Type of the sensor
     * @return Return sensor build
     */
    public Sensor create(int id,String name,String type){
        //Create alcohol sensor
        if(type.equals("Alcohol")) return createAlcoholSensor(id,name);

        //Create carbon sensor
        else if(type.equals("Carbon")) return createCarbonSensor(id,name);

        //Create gas sensor
        else if(type.equals("Gas")) return createGasSensor(id,name);

        //Create humidity sensor
        else if(type.equals("Humidity")) return createHumiditySensor(id,name);

        //Create light sensor
        else if(type.equals("Light")) return createLightSensor(id,name);

        //Create methane sensor
        else if(type.equals("Methane")) return createMethaneSensor(id,name);

        //Create propane sensor
        else if(type.equals("Propane")) return createPropaneSensor(id,name);

        //Create sound sensor
        else if(type.equals("Sound")) return createSoundSensor(id,name);
        return null;
    }

    /**
     * Create alcohol sensor
     * @param id Id of the sensor
     * @param name Name of the sensor
     * @return Return the alcohol sensor build
     */
    public Alcohol createAlcoholSensor(int id,String name){return new Alcohol(id,name);}

    /**
     * Create carbon sensor
     * @param id Id of the sensor
     * @param name Name of the sensor
     * @return Return the carbon sensor build
     */
    public Carbon createCarbonSensor(int id, String name){return new Carbon(id,name);}

    /**
     * Create gas sensor
     * @param id Id of the sensor
     * @param name Name of the sensor
     * @return Return the gas sensor build
     */
    public Gas createGasSensor(int id,String name){return new Gas(id,name);}

    /**
     * Create humidity sensor
     * @param id Id of the sensor
     * @param name Name of the sensor
     * @return Return the humidity sensor build
     */
    public Humidity createHumiditySensor(int id,String name){return new Humidity(id,name);}

    /**
     * Create light sensor
     * @param id Id of the sensor
     * @param name Name of the sensor
     * @return Return the light sensor build
     */
    public Light createLightSensor(int id, String name){return new Light(id,name);}

    /**
     * Create methane sensor
     * @param id Id of the sensor
     * @param name Name of the sensor
     * @return Return the methane sensor build
     */
    public Methane createMethaneSensor(int id,String name){return new Methane(id,name);}

    /**
     * Create propane sensor
     * @param id Id of the sensor
     * @param name Name of the sensor
     * @return Return the propane sensor build
     */
    public Propane createPropaneSensor(int id,String name){return new Propane(id,name);}

    /**
     * Create sound sensor
     * @param id Id of the sensor
     * @param name Name of the sensor
     * @return Return the sound sensor build
     */
    public Sound createSoundSensor(int id,String name){return new Sound(id,name);}
}
