package com.arduino.sensors;


/**
 * This class represent a Carbon sensor
 */
public class Carbon extends Gas
{
    public Carbon(int id, String name) {
        super(id, name,"Carbon");
    }
}

