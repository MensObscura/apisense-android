package com.arduino.sensors;

/**
 * This class represent a Sound sensor
 */
public class Sound extends Sensor
{
    public Sound(int id, String name) {
        super(id, name,"Sound");
    }
}

