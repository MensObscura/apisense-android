package com.arduino.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.arduino.database.tables.ModulesAndSensorsAssociationsTable;
import com.arduino.database.tables.ModulesTable;
import com.arduino.database.tables.SensorsTable;
import com.arduino.database.tables.TableFactory;

import java.io.Serializable;

/**
 * Created by jerome on 24/10/14.
 * This class will create and update all tables of the database
 */
public class SQLiteHelper extends SQLiteOpenHelper implements Serializable{
    public final static int VERSION_DB = 1;
    public final static String NAME_DB = "arduinoSDK.db";
    public TableFactory tableFactory;

    public SQLiteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version,TableFactory tableFactory) {
        super(context, name, factory, version);
        this.tableFactory=tableFactory;
    }

    /**
     * Create all tables in the database.
     * @param db the database
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        tableFactory.getModuleTable().onCreate(db);
        tableFactory.getSensorTable().onCreate(db);
        tableFactory.getAssociationTable().onCreate(db);

    }

    /**
     * Delete all tables and create a new database
     * @param db the databse
     * @param i
     * @param i2
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i2) {
        tableFactory.getModuleTable().onUpgrade(db);
        tableFactory.getSensorTable().onUpgrade(db);
        tableFactory.getAssociationTable().onUpgrade(db);
        onCreate(db);
    }
}
