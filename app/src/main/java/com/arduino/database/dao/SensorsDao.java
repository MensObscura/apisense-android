package com.arduino.database.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.arduino.database.SQLiteHelper;
import com.arduino.database.tables.ModulesAndSensorsAssociationsTable;
import com.arduino.database.tables.SensorsTable;

import com.arduino.module.ModuleArduino;
import com.arduino.sensors.Sensor;
import com.arduino.sensors.SensorBuilder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jerome on 24/10/14.
 * Class for the data access object of Sensors.
 */
public class SensorsDao extends AbstractDao{

    private SensorBuilder sensorBuilder;

    // all columns of the table
    private String[] allColumns = {SensorsTable.COL_ID, SensorsTable.COL_NAME, SensorsTable.COL_TYPE};

    public SensorsDao(SQLiteHelper helper,SensorBuilder sensorBuilder) {
        super(helper);
        this.sensorBuilder=sensorBuilder;
    }

    /**
     * Insert a new Sensor into the database
     * @param sensor the new sensor
     * @return
     */
    public long insertSensor(Sensor sensor) throws DaoException {
        if(!isOpen())
            throw new DaoException("Database not open");
        ContentValues values = new ContentValues();
        values.put(SensorsTable.COL_ID,sensor.getId());
        values.put(SensorsTable.COL_NAME, sensor.getName());
        values.put(SensorsTable.COL_TYPE, sensor.getType());
        return db.insert(SensorsTable.TABLE_SENSORS, null, values);
    }

    /**
     * Update informations about a Sensor into the database
     * @param sensor the sensor with new informations
     * @return
     */
    public int updateSensor(Sensor sensor) throws DaoException {
        if(!isOpen())
            throw new DaoException("Database not open");
        ContentValues values = new ContentValues();
        values.put(SensorsTable.COL_NAME, sensor.getName());
        values.put(SensorsTable.COL_TYPE, sensor.getType());
        return db.update(SensorsTable.TABLE_SENSORS, values, SensorsTable.COL_ID + " = "+sensor.getId(), null);
    }

    /**
     * Remove a Sensor from the database
     * @param id the id of the sensor to delete
     * @return
     */
    public int removeSensor(int id) throws DaoException {
        if(!isOpen())
            throw new DaoException("Database not open");
        return db.delete(SensorsTable.TABLE_SENSORS, SensorsTable.COL_ID + " = "+id, null);
    }

    /**
     * Select a Sensor from the database
     * @param name the name of the sensor that we want
     * @return the Sensor which match with the name
     */
    public Sensor getSensor(String name) throws DaoException {
        if(!isOpen())
            throw new DaoException("Database not open");
        Cursor c = db.query(SensorsTable.TABLE_SENSORS, allColumns, SensorsTable.COL_NAME + " LIKE \""+name+"\"",null, null, null, null);
        return cursorToSensor(c);
    }

    /**
     * Convert the cursor's information into a Sensor object
     * @param c the cursor returned by the query to the database
     * @return the Sensor object
     */
    private Sensor cursorToSensor(Cursor c) {
        if (c.getCount() == 0) {
            return null;
        }
        c.moveToFirst();

        //Get the sensor id
        int id=c.getInt(SensorsTable.NUM_COL_ID);

        //Get the sensor name
        String name=c.getString(SensorsTable.NUM_COL_NAME);

        //Get the sensor type
        String type = c.getString(SensorsTable.NUM_COL_TYPE);

        //Create the sensor
        Sensor sensor =  sensorBuilder.create(id,name,type);
        c.close();
        return sensor;
    }

    /**
     * Select all Sensor in the database
     * @return all Sensor in the database
     */
    public List<Sensor> getSensors() throws DaoException {
        if(!isOpen())
            throw new DaoException("Database not open");
        List<Sensor> sensors=new ArrayList<Sensor>();
        Cursor c = db.query(SensorsTable.TABLE_SENSORS, allColumns, null,null, null, null, null);
        if (c.getCount() == 0) {
            return sensors;
        }
        c.moveToFirst();
        do {
            //Get the sensor id
            int id = c.getInt(SensorsTable.NUM_COL_ID);

            //Get the sensor name
            String name = c.getString(SensorsTable.NUM_COL_NAME);

            //Get the sensor type
            String type = c.getString(SensorsTable.NUM_COL_TYPE);

            //Create the sensor
            Sensor module = sensorBuilder.create(id, name,type);

            //Add the sensor to the list
            sensors.add(module);
            c.moveToNext();
        }while(!c.isAfterLast());
        c.close();
        return sensors;
    }

    /**
     * Select all Sensor associated to a module
     * @param modules Module for which select all sensor
     * @return all Sensor in the database
     */
    public List<Sensor> getSensors(ModuleArduino modules) throws DaoException {
        if(!isOpen())
            throw new DaoException("Database not open");
        List<Sensor> sensors=new ArrayList<Sensor>();

        //SQL query for select all sensor associated to a module
        String sql="SELECT s."+SensorsTable.COL_ID+", s."+SensorsTable.COL_NAME+", s."+SensorsTable.COL_TYPE
                +" FROM "+SensorsTable.TABLE_SENSORS+" AS s INNER JOIN "+ ModulesAndSensorsAssociationsTable.TABLE_ASSOCIATIONS+" AS m"
                +" ON m."+ModulesAndSensorsAssociationsTable.COL_SENSOR_ID+" = s."+SensorsTable.COL_ID
                +" WHERE m."+ModulesAndSensorsAssociationsTable.COL_MODULE_ID+"="+modules.getId();

        //Execute the query
        Cursor c=db.rawQuery(sql,null);

        if (c.getCount() == 0) {
            return null;
        }
        c.moveToFirst();

        do {
            //Get the sensor id
            int id = c.getInt(SensorsTable.NUM_COL_ID);

            //Get the sensor name
            String name = c.getString(SensorsTable.NUM_COL_NAME);

            //Get the sensor type
            String type = c.getString(SensorsTable.NUM_COL_TYPE);

            //Create the sensor
            Sensor module = sensorBuilder.create(id, name,type);

            //Add the sensor to the list
            sensors.add(module);
            c.moveToNext();
        }while(!c.isAfterLast());
        c.close();
        return sensors;
    }

    /**
     * Remove all sensor in the table table_sensors
     * @throws DaoException
     */
    public void clean() throws DaoException {
        if(!isOpen())
            throw new DaoException("Database not open");
        db.delete(SensorsTable.TABLE_SENSORS,null, null);
    }
}
