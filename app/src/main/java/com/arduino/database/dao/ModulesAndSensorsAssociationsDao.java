package com.arduino.database.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.arduino.database.SQLiteHelper;
import com.arduino.database.tables.ModulesAndSensorsAssociationsTable;
import com.arduino.database.tables.SensorsTable;
import com.arduino.database.tables.TableFactory;

/**
 * Created by jerome on 24/10/14.
 * Class for the data access object of Associations between Modules and Sensors
 */
public class ModulesAndSensorsAssociationsDao extends AbstractDao{

    // all columns of the table
    private String[] allColumns = {ModulesAndSensorsAssociationsTable.COL_ID, ModulesAndSensorsAssociationsTable.COL_MODULE_ID, ModulesAndSensorsAssociationsTable.COL_SENSOR_ID};

    public ModulesAndSensorsAssociationsDao(SQLiteHelper helper) {
        super(helper);
    }


    /**
     * Insert a new Association into the database
     * @param module a module
     * @param sensor the sensor to link with the module
     * @return
     */
    public long insertAssociation(int module, int sensor) throws DaoException {
        if(!isOpen())
            throw new DaoException("Database not open");
        ContentValues values = new ContentValues();
        values.put(ModulesAndSensorsAssociationsTable.COL_MODULE_ID, module);
        values.put(ModulesAndSensorsAssociationsTable.COL_SENSOR_ID, sensor);
        return db.insert(ModulesAndSensorsAssociationsTable.TABLE_ASSOCIATIONS, null, values);
    }

    /**
     * Remove an Association from the database
     * @param id the id of the Association to delete
     * @return
     */
    public int removeAssociation(int id) throws DaoException {
        if(!isOpen())
            throw new DaoException("Database not open");
        return db.delete(ModulesAndSensorsAssociationsTable.TABLE_ASSOCIATIONS, ModulesAndSensorsAssociationsTable.COL_ID + " = "+id, null);
    }

    /**
     * Remove all association in the table table_associations
     * @throws DaoException
     */
    public void clean() throws DaoException {
        if(!isOpen())
            throw new DaoException("Database not open");
        db.delete(ModulesAndSensorsAssociationsTable.TABLE_ASSOCIATIONS,null, null);
    }
}
