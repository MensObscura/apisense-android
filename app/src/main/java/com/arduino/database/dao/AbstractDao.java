package com.arduino.database.dao;

import android.database.sqlite.SQLiteDatabase;

import com.arduino.database.SQLiteHelper;

public abstract class AbstractDao {

    // database
    protected SQLiteDatabase db;

    // our database helper
    protected SQLiteHelper dbHelper;

    public AbstractDao(SQLiteHelper helper) {
        dbHelper = helper;
    }

    /**
     * Open connection to the database
     */
    public void open() {
        if(!isOpen()) {
            db = dbHelper.getWritableDatabase();
        }
    }

    /**
     * Close the connection to the database
     */
    public void close() {
        if(isOpen())
            db.close();
    }

    public SQLiteDatabase getDB() {
        return db;
    }

    public boolean isOpen(){
        return (db!=null && db.isOpen());
    }

    /**
     * Remove all into a table
     */
    public abstract void clean() throws DaoException;
}
