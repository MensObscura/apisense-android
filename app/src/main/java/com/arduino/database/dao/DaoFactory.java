package com.arduino.database.dao;

import android.content.Context;

import com.arduino.database.SQLiteHelper;
import com.arduino.module.ModuleArduinoBuilder;
import com.arduino.sensors.SensorBuilder;

import java.io.Serializable;

/**
 * This class represent a factory. This factory create instance for all dao
 */
public class DaoFactory{

    private SQLiteHelper helper;
    private ModuleArduinoBuilder moduleBuilder;
    private SensorBuilder sensorBuilder;

    public DaoFactory(SQLiteHelper helper,ModuleArduinoBuilder moduleBuilder,SensorBuilder sensorBuilder){
        this.helper=helper;
        this.moduleBuilder=moduleBuilder;
        this.sensorBuilder=sensorBuilder;
    }

    /**
     * This method return the dao for sensors
     * @return Return the dao for sensors
     */
    public SensorsDao getSensorDao(){
        SensorsDao sensorDao=new SensorsDao(helper,sensorBuilder);
        sensorDao.open();
        return sensorDao;
    }

    /**
     * This method return the dao for modules
     * @return Return the dao for modules
     */
    public ModulesDao getModuleDao(){
        ModulesDao moduleDao=new ModulesDao(helper,moduleBuilder);
        moduleDao.open();
        return moduleDao;
    }

    /**
     * This method return the dao for association between modules arduino and sensors
     * @return Return the dao for association between modules arduino and sensors
     */
    public ModulesAndSensorsAssociationsDao getAssociationDao(){
        ModulesAndSensorsAssociationsDao assoDao=new ModulesAndSensorsAssociationsDao(helper);
        assoDao.open();
        return assoDao;
    }
}
