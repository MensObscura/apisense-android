package com.arduino.database.dao;

import android.content.ContentValues;
import android.database.Cursor;

import com.arduino.database.SQLiteHelper;
import com.arduino.database.tables.ModulesTable;
import com.arduino.module.ModuleArduino;
import com.arduino.module.ModuleArduinoBuilder;

/**
 * Created by jerome on 24/10/14.
 * Class for the data access object of Modules
 */
public class ModulesDao extends AbstractDao{

    private ModuleArduinoBuilder moduleBuilder;

    // all columns of the table
    private String[] allColumns = {ModulesTable.COL_ID, ModulesTable.COL_ADDRESS};

    public ModulesDao(SQLiteHelper helper,ModuleArduinoBuilder moduleBuilder) {
        super(helper);
        this.moduleBuilder=moduleBuilder;
    }

    /**
     * Insert a new Module into the database
     * @param module the new module
     * @return
     */
    public long insertModule(ModuleArduino module) throws DaoException {
        if(!isOpen())
            throw new DaoException("Database not open");
        ContentValues values = new ContentValues();
        values.put(ModulesTable.COL_ID,module.getId());
        values.put(ModulesTable.COL_ADDRESS, module.getAddress());
        return db.insert(ModulesTable.TABLE_MODULES, null, values);
    }

    /**
     * Update informations about a Module into the database
     * @param module the module with new informations
     * @return
     */
    public int updateModule(ModuleArduino module) throws DaoException {
        if(!isOpen())
            throw new DaoException("Database not open");
        ContentValues values = new ContentValues();
        values.put(ModulesTable.COL_ADDRESS, module.getAddress());
        return db.update(ModulesTable.TABLE_MODULES, values, ModulesTable.COL_ID + " = "+module.getId(), null);
    }

    /**
     * Remove a Module from the database
     * @param id the id of the module to delete
     * @return
     */
    public int removeModule(int id) throws DaoException {
        if(!isOpen())
             throw new DaoException("Database not open");
        return db.delete(ModulesTable.TABLE_MODULES, ModulesTable.COL_ID + " = "+id, null);
    }

    /**
     * Select a Module from the database
     * @param address the address of the module that we want
     * @return the Module which match with the address
     */
    public ModuleArduino getModule(String address) throws DaoException {
        if(!isOpen())
            throw new DaoException("Database not open");
        Cursor c = db.query(ModulesTable.TABLE_MODULES, allColumns, ModulesTable.COL_ADDRESS + " LIKE \""+address+"\"",null, null, null, null);
        return cursorToModule(c);
    }

    /**
     * Convert the cursor's information into a ModuleArduino object
     * @param c the cursor returned by the query to the database
     * @return the ModuleArduino object
     */
    private ModuleArduino cursorToModule(Cursor c) {
        if (c.getCount() == 0) {
            return null;
        }
        c.moveToFirst();
        int id=c.getInt(ModulesTable.NUM_COL_ID);
        String address=c.getString(ModulesTable.NUM_COL_ADDRESS);
        ModuleArduino module =  moduleBuilder.createModuleArduino(id, address);
        c.close();
        return module;
    }

    /**
     * Remove all module in the table table_modules
     * @throws DaoException
     */
    public void clean() throws DaoException {
        if(!isOpen())
            throw new DaoException("Database not open");
        db.delete(ModulesTable.TABLE_MODULES,null, null);
    }
}
