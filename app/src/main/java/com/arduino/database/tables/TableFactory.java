package com.arduino.database.tables;

import android.content.Context;

import java.io.Serializable;

public class TableFactory{
    public SensorsTable getSensorTable(){
        return new SensorsTable();
    }

    public ModulesTable getModuleTable(){
        return new ModulesTable();
    }

    public ModulesAndSensorsAssociationsTable getAssociationTable(){
        return new ModulesAndSensorsAssociationsTable();
    }
}
