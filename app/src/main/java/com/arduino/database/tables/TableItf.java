package com.arduino.database.tables;

import android.database.sqlite.SQLiteDatabase;

public interface TableItf {
    public void onCreate(SQLiteDatabase db);
    public void onUpgrade(SQLiteDatabase db);
}
