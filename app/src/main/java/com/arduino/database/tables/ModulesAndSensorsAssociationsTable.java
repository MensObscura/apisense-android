package com.arduino.database.tables;

import android.database.sqlite.SQLiteDatabase;

/**
 * Created by jerome on 24/10/14.
 * Class for the association between Modules and Sensors.
 */
public class ModulesAndSensorsAssociationsTable implements TableItf{

    // Name of the table
    public static final String TABLE_ASSOCIATIONS = "table_associations";

    // Columns name and columns ID of the table
    public static final String COL_ID = "ID";
    public static final int NUM_COL_ID = 0;
    public static final String COL_MODULE_ID = "MODULE_ID";
    public static final int NUM_COL_MODULE_ID = 1;
    public static final String COL_SENSOR_ID = "SENSOR_ID";
    public static final int NUM_COL_SENSOR_ID = 2;

    // Query for the creation of the table
    private static final String CREATE_DB = "CREATE TABLE "+ TABLE_ASSOCIATIONS + " ("+ COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "+ COL_MODULE_ID +" INTEGER NOT NULL, "+ COL_SENSOR_ID +" INTEGER NOT NULL);";

    /**
     * Create the Associations table.
     * @param db the database
     */
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_DB);
    }

    /**
     * Delete the Association table.
     * @param db the database
     */
    public void onUpgrade(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ASSOCIATIONS + ";");
    }
}
