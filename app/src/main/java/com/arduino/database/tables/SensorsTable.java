package com.arduino.database.tables;

import android.database.sqlite.SQLiteDatabase;

/**
 * Created by jerome on 24/10/14.
 * Class for the Sensors table
 */
public class SensorsTable implements TableItf {

    // Name of the table
    public static final String TABLE_SENSORS = "table_sensors";

    // Columns name, columns ID and columns type of the table
    public static final String COL_ID = "ID";
    public static final int NUM_COL_ID = 0;
    public static final String COL_NAME = "NAME";
    public static final int NUM_COL_NAME = 1;
    public static final String COL_TYPE = "TYPE";
    public static final int NUM_COL_TYPE = 2;


    // Query for the creation of the table
    private static final String CREATE_DB = "CREATE TABLE "+ TABLE_SENSORS + " ("+ COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "+ COL_NAME + " TEXT NOT NULL, "+COL_TYPE+" TEXT NOT NULL);";

    /**
     * Create the Sensors table.
     * @param db the database
     */
    public void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_DB);
    }

    /**
     * Delete the Sensors table.
     * @param db the database
     */
    public void onUpgrade(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SENSORS + ";");
    }
}
