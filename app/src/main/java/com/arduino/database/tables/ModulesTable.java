package com.arduino.database.tables;

import android.database.sqlite.SQLiteDatabase;

/**
 * Created by jerome on 24/10/14.
 * Class for the Modules table
 */
public class ModulesTable implements TableItf{

    // Name of the table
    public static final String TABLE_MODULES = "table_modules";

    // Columns name and columns ID of the table
    public static final String COL_ID = "ID";
    public static final int NUM_COL_ID = 0;
    public static final String COL_ADDRESS = "ADDRESS";
    public static final int NUM_COL_ADDRESS = 1;

    // Query for the creation of the table
    private static final String CREATE_DB = "CREATE TABLE "+ TABLE_MODULES + " ("+ COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "+ COL_ADDRESS + " TEXT NOT NULL);";

    /**
     * Create the Modules table.
     * @param db the database
     */
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_DB);
    }

    /**
     * Delete the Module table.
     * @param db the database
     */
    public void onUpgrade(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MODULES + ";");
    }
}
