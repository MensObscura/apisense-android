package com.arduino.dialog;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.arduino.R;
import com.arduino.activite.App;
import com.arduino.dialog.listener.CancelDialog;
import com.arduino.dialog.listener.ValidateAddServer;

/**
 * This class represent a dialog can edit the server uri
 */
@SuppressLint("ValidFragment")
public class AddServerDialog extends DialogFragment {

    private App app;

    public AddServerDialog(App app){
        super();
        this.app=app;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        //Get the alert dialog  builder
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();

        //Get the custom view
        View custom_layout=inflater.inflate(R.layout.add_serveur_layout, null);

        //Get the edit text for the server uri and set this value
        EditText addr=(EditText)custom_layout.findViewById(R.id.menu_addr_server);
        addr.setText(app.getServeur());

        //Set the title of the dialog
        builder.setTitle(R.string.server);

        //Set the custom view with buttons
        builder.setView(custom_layout)
                .setPositiveButton(R.string.validate, new ValidateAddServer(app,addr))
                .setNegativeButton(R.string.cancel, new CancelDialog());


        //Create dialog
        return builder.create();
    }


}
