package com.arduino.dialog.listener;

import android.content.DialogInterface;

/**
 * This class represent the action to click on a cancel button in a dialog frame. The dialog is cancel
 */
public class CancelDialog implements DialogInterface.OnClickListener {

    public void onClick(DialogInterface dialog, int id) {
        dialog.cancel();
    }
}
