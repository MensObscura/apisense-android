package com.arduino.dialog.listener;

import android.content.DialogInterface;
import android.widget.EditText;

import com.arduino.activite.App;

/**
 * This class represent the action to click on a validate button in a dialog frame for edit the server uri. This action save the server uri and cancel the dialog
 */
public class ValidateAddServer implements DialogInterface.OnClickListener {

    private App app;
    private EditText addr;

    public ValidateAddServer(App app,EditText addr){
        this.app=app;
        this.addr=addr;
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {

        //Save the server uri
        app.setServeur(addr.getText().toString());

        //Cancel the dialog
        dialogInterface.cancel();
    }
}
