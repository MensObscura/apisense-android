package com.arduino.dialog.listener;

import android.content.DialogInterface;
import android.widget.RadioGroup;

import com.arduino.R;
import com.arduino.activite.App;
import com.arduino.module.Mode;

/**
 * This class represent the action to click on a mode button in a dialog frame for change the mode of communication.
 */
public class ValidateChangeMode implements DialogInterface.OnClickListener {

    private App app;
    private RadioGroup groupBtn;

    public ValidateChangeMode(App app,RadioGroup groupBtn){
        this.app=app;
        this.groupBtn=groupBtn;
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        if(groupBtn.getCheckedRadioButtonId() == R.id.menu_mode_pull){
            app.setMode(Mode.PULL);
        }
        else{
            app.setMode(Mode.PUSH);
        }

        //Cancel the dialog
        dialogInterface.cancel();
    }
}
