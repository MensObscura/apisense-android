package com.arduino.dialog;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.arduino.R;
import com.arduino.activite.App;
import com.arduino.dialog.listener.CancelDialog;
import com.arduino.dialog.listener.ValidateChangeMode;
import com.arduino.module.Mode;

/**
 * This class represent a dialog change the communication mode
 */
@SuppressLint("ValidFragment")
public class ChangeModeDialog extends DialogFragment {

    private App app;

    public ChangeModeDialog(App app){
        super();
        this.app=app;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //Get the alert dialog  builder
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();

        //Set the title of the dialog
        builder.setTitle(R.string.mode);

        //Get the custom view
        View custom_layout=inflater.inflate(R.layout.change_mode_layout, null);

        //Get radio group
        RadioGroup groupBtn=(RadioGroup)custom_layout.findViewById(R.id.menu_change_mode);

        //Check the mode selected
        if(app.getMode().equals(Mode.PULL)){
            groupBtn.check(R.id.menu_mode_pull);
        }
        else{
            groupBtn.check(R.id.menu_mode_push);
        }

        //Set the custom view with buttons
        builder.setView(custom_layout)
                .setPositiveButton(R.string.validate, new ValidateChangeMode(app,groupBtn))
                .setNegativeButton(R.string.cancel, new CancelDialog());

        //Create dialog
        return builder.create();
    }
}
