package com.arduino.link;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.arduino.R;
import com.arduino.activite.AcceptBluetoothActivity;
import com.arduino.activite.App;
import com.arduino.activite.ShowResult;
import com.arduino.module.Mode;
import com.arduino.module.SystemModuleArduino;

import java.util.TimerTask;
import java.util.UUID;

/**
 * This class detect bluetooth devices and if it's an arduino authorized by the server connect it to the phone
 */
public class ConnectTask extends TimerTask {
    private SystemModuleArduino systemModuleArduino;
    private Mode mode;
    private UUID uuid;
    private Context ctx;
    private App app;
    private BluetoothAdapter mBluetoothAdapter;
    private int notif;


    public ConnectTask(SystemModuleArduino systemModuleArduino,Mode mode,UUID uuid,Context ctx,App app){
        this.systemModuleArduino=systemModuleArduino;
        this.mode=mode;
        this.uuid=uuid;
        this.ctx=ctx;
        this.app=app;
        notif=0;
        addReceiver();
        mBluetoothAdapter=BluetoothAdapter.getDefaultAdapter();
    }

    //BroadcastReceiver for the filter BluetoothDevice.ACTION_FOUND
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {

            //Get the action
            String action = intent.getAction();

            //If action is equal to BluetoothDevice.ACTION_FOUND
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {

                // Get the BluetoothDevice object from the Intent
                final BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                //If the device is know
                if(canGetData(device)){
                    //If the mode of communication is PUSH
                    if(mode.equals(Mode.PUSH)) {
                        //Start ShowResult activity for show data received
                        Intent intent2=new Intent(context,ShowResult.class);
                        intent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        app.startActivity(intent2);

                        //Start the communication with the module
                        app.receiveData(device);
                    }

                    //If the mode of communication is PULL
                    else if(mode.equals(Mode.PULL)){

                        //Send a notification
                        sendNotification(device);
                    }
                }
            }
        }
    };

    /**
     * Add the receiver for detect bluetooth devices
     */
    protected void addReceiver(){
        //Create a new intentFilter
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);

        //Register the new intentFilter and her handler
        app.registerReceiver(mReceiver, filter);
    }

    /**
     * Send a notification for users accept communication with the module
     * @param device Device represent the module.
     */
    protected void sendNotification(BluetoothDevice device){
        //Get a builder for communication
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(ctx);

        //Set the icon to the notification
        mBuilder.setSmallIcon(R.drawable.icon);

        //Set the title to the notification
        mBuilder.setContentTitle("Module arduino détecté");

        //Set the text of the notification
        mBuilder.setContentText(device.getName()+" a été détecté à proximité");

        //If click on the notification then start an activity for accept the communication
        Intent resultIntent = new Intent(app.getBaseContext(), AcceptBluetoothActivity.class);

        resultIntent.putExtra("Bluetooth",device);

        TaskStackBuilder  stackBuilder = TaskStackBuilder.create(app.getBaseContext());
        stackBuilder.addParentStack(AcceptBluetoothActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);

        //Send the notification
        NotificationManager mNotificationManager =(NotificationManager) app.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(notif++, mBuilder.build());
    }

    /**
     * This method says if we can get data from a device
     * @param device Bluetooth device can know if can get data
     * @return Return true if can get data from this device, otherwise false
     */
    protected boolean canGetData(BluetoothDevice device){
        if(device != null && device.getAddress()!=null){
            return systemModuleArduino.knowModule(device.getAddress());
        }
        return false;
    }

    public void setMode(Mode mode){
        this.mode=mode;
    }

    @Override
    public void run() {
        if (mBluetoothAdapter != null) {
            if (mBluetoothAdapter.isEnabled()) {
                //Start discovery of device
                mBluetoothAdapter.startDiscovery();
            }
        }
    }
}