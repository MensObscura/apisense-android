package com.arduino.link;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.util.Log;

import com.arduino.module.ModuleArduino;
import com.arduino.module.SystemModuleArduino;
import com.arduino.sensors.Sensor;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.List;
import java.util.Observable;
import java.util.UUID;

/**
 * This class is used for communicate between the arduino device and the android application
 */
public class SocketArduino extends Observable implements Runnable {

    protected BluetoothDevice device;
    protected SystemModuleArduino systemModuleArduino;
    protected BufferedWriter bos;
    protected InputStream is;

    public SocketArduino(BluetoothDevice device,SystemModuleArduino systemModuleArduino){
        this.device=device;
        this.systemModuleArduino=systemModuleArduino;
    }

    @Override
    public void run() {


        BluetoothSocket socket=null;
        try{
            //Get the socket with the arduino device
            //socket=device.createRfcommSocketToServiceRecord(uuid);
            socket = device.createRfcommSocketToServiceRecord(UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"));

            //Connect the android application to the arduino device
            socket.connect();
            try {

                //Get the input and output stream
                OutputStream os=socket.getOutputStream();
                bos=new BufferedWriter(new OutputStreamWriter(os));
                is=socket.getInputStream();

                //Get the arduino device
                ModuleArduino module=systemModuleArduino.getModule(device.getAddress());

                //Get sensors associated to the device
                List<Sensor> sensors=module.getListSensor();

                //Get sensors available
                List<Integer> availableSensor=systemModuleArduino.getAvailableSensor();

                //Foreach sensors associated to the device
                for(Sensor s:sensors) {
                    //If the sensor is available
                    if (availableSensor.contains(s.getId())){
                        //Send a request for the sensor
                        sendRequest(s.getName());

                        //Start a timer for stop reading after 5 seconds
                        MyRun r=new MyRun();
                        Thread t=new Thread();
                        t.start();

                        //Read data received by the arduino for this sensor
                        double data=Double.parseDouble(readData());

                        //Set data received to the sensor class
                        s.setData(data);

                        //Notify data received
                        Log.d("msgArduino",data+"");
                        systemModuleArduino.receiveData(s);

                        //Stop the timer
                        r.run=false;
                    }
                }

                //Close input and outputstream and the socket
                is.close();
                bos.close();
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }

    /**
     * Send a request to the device
     * @param request
     */
    public void sendRequest(String request){
        try {
            bos.write("<"+request+">");
            bos.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Read data received froma device
     * @return
     * @throws IOException
     */
    public String readData() throws IOException{
        int read = -1;
        boolean canRead=false;
        final byte[] bytes = new byte[1024];
        StringBuilder b = new StringBuilder();
        char c='#';

        //While the inputstream is open
        for (;(read = is.read(bytes)) > -1;) {

            final int count = read;

            //Foreach byte read
            for (int i = 0; i < count; ++i) {

                //Convert the numbre received to a char
                String s = Integer.toString(bytes[i]);
                c=(char)Integer.parseInt(s);


                if(!canRead){
                    if('<' == c){
                        canRead=true;
                    }
                }
                else{
                    if('>' == c){
                        break;
                    }
                    else{
                        b.append(c);
                    }
                }
            }
            if('>' == c){
                break;
            }
        }
        return b.toString();
    }

    public class MyRun implements Runnable{
        public boolean run=true;

        @Override
        public void run() {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if(run) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
