package com.arduino.link;

import android.content.Context;
import android.util.Log;

import com.arduino.activite.App;
import com.arduino.module.Mode;
import com.arduino.module.SystemModuleArduino;

import java.util.Timer;
import java.util.UUID;

/**
 * This class is used to detect bluetooth devices all of the 10 seconds
 */
public class ConnectModule{
    private Timer timer;
    private SystemModuleArduino systemModuleArduino;
    private Mode mode;
    private UUID uuid;
    private ConnectTask connectTask;

    public ConnectModule(SystemModuleArduino sysArduino,Mode mode,Context ctx,App app){
        systemModuleArduino=sysArduino;
        this.mode=mode;
        uuid=UUID.randomUUID();
        timer=new Timer();
        connectTask=new ConnectTask(systemModuleArduino,mode,uuid,ctx,app);
    }

    public Mode getMode(){
        return mode;
    }

    public void start(){
        timer.scheduleAtFixedRate(connectTask,0,10000);
    }

    public void stop(){
        timer.cancel();
    }


    public void setMode(Mode mode) {
        this.mode=mode;
        connectTask.setMode(mode);
    }
}
