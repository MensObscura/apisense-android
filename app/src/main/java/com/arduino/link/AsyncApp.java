package com.arduino.link;

import android.os.AsyncTask;

import com.arduino.module.SystemModuleArduino;

/**
 * This class execute all of the asynchrone task ( load data information into the server before connect arduino to the phone )
 */
public class AsyncApp extends AsyncTask<Void, Void, Void> {

    private SystemModuleArduino systemModuleArduino;
    private ConnectModule connectModule;

    public AsyncApp(SystemModuleArduino systemModuleArduino,ConnectModule connectModule){
        this.systemModuleArduino=systemModuleArduino;
        this.connectModule=connectModule;
    }

    protected Void doInBackground(Void... voids) {
        //Load data information into the server
        systemModuleArduino.load();

        //Connect arduino to the phone
        connectModule.start();
        return null;
    }
}
