package com.arduino.module;

public class JSONParserException extends Exception {
    public JSONParserException(String msg){
        super(msg);
    }
}
