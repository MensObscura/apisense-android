package com.arduino.module;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;
import android.util.Log;

/**
 * Created by jerome on 17/10/14.
 * This class will help to parse the JSON object received from the server.
 */

public class JSONParser implements Serializable{
    public JSONParser() {
    }

    /**
     * Connect to the server URL and retrieve the JSON Object
     * @param url the URL of the server
     * @return a JSONObject which will help to retrieve the informations sent by the server.
     */
    public JSONObject getJSONFromUrl(String url) throws JSONParserException {
        InputStream is=connectHTTP(url);
        String json = findData(is);
        JSONObject jObj=new JSONObject();
        // try parse the string to a JSON object
        try {
            jObj = new JSONObject(json);
        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }
        // return JSON String
        return jObj;
    }

    /**
     * Call an http request and return inputstream to the response
     * @param url Url of the website
     * @return Return an inputstream to the response
     * @throws JSONParserException
     */
    protected InputStream connectHTTP(String url) throws JSONParserException {
        // Making HTTP request
        try {
            // defaultHttpClient
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);
            HttpResponse httpResponse = httpClient.execute(httpPost);
            HttpEntity httpEntity = httpResponse.getEntity();
            return httpEntity.getContent();
        } catch (Exception e){
            String str="-----------------------\r\n";
            str+=e.getMessage()+"\r\n";
            for(StackTraceElement elt:e.getStackTrace()){
                str+=elt.toString()+"\r\n";
            }
            str+="-----------------------\r\n";
            throw new JSONParserException("Error connect to the server:\r\n"+str);
        }
    }

    /**
     * Find all data from the response of the http call
     * @param is InputStream to the response
     * @return Data in the response
     */
    protected String findData(InputStream is){
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            return sb.toString();
        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }
        return "";
    }
}