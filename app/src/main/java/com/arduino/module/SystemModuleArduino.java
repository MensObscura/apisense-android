package com.arduino.module;

import android.util.Log;

import com.arduino.database.dao.DaoException;
import com.arduino.database.dao.DaoFactory;
import com.arduino.database.dao.ModulesAndSensorsAssociationsDao;
import com.arduino.database.dao.ModulesDao;
import com.arduino.database.dao.SensorsDao;
import com.arduino.sensors.Sensor;
import com.arduino.sensors.SensorBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Observable;

/**
 * This class synchronize the informations about Modules and Sensors between the server and the
 * telephone.
 */
public class SystemModuleArduino extends Observable {

    private static final String TAG_MODULE="module";
    private static final String TAG_SENSOR="sensor";
    private static final String TAG_ASSO="asso";

    // URL to get JSON
    private String url;

    public DaoFactory daoFactory;
    private ModuleArduinoBuilder moduleBuilder;
    private SensorBuilder sensorBuilder;
    private JSONParser jParser;
    private List<Integer> availableSensor;

    // Singleton
    private static SystemModuleArduino INSTANCE;

    protected SystemModuleArduino(String url,List<Integer> availableSensor,DaoFactory daoFactory,ModuleArduinoBuilder moduleBuilder,SensorBuilder sensorBuilder,JSONParser parser) {
        this.daoFactory=daoFactory;
        this.moduleBuilder=moduleBuilder;
        this.sensorBuilder=sensorBuilder;
        this.jParser=parser;
        this.availableSensor=availableSensor;
        setURL(url);
    }

    /**
     * Get the Singleton instance of SystemModuleArduino
     * @return the single instance of SystemModuleArduino
     */
    public static SystemModuleArduino getInstance(String url,List<Integer> availableSensor,DaoFactory daoFactory,ModuleArduinoBuilder moduleBuilder,SensorBuilder sensorBuilder,JSONParser parser){
        if(daoFactory!=null) {
            if (INSTANCE == null) {
                INSTANCE = new SystemModuleArduino(url, availableSensor, daoFactory, moduleBuilder, sensorBuilder, parser);
            }
            return INSTANCE;
        }
        return null;
    }

    public String getUrl() {
        return this.url;
    }

    public void setURL(String url) {
        this.url = url;
    }

    /**
     * Connect to the server, get all Modules with their Sensors, and store them into the local
     * database.
     */
    public void load() {

        // Getting JSON from URL
        final JSONObject[] json = {null};

        //Read json with list of sensors and arduino device know
        Thread t=new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    json[0] = jParser.getJSONFromUrl(url);
                } catch (JSONParserException e) {

                }
            }
        });
        t.start();
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //If json is received
        if(json[0]!=null) {
            //Clean the database
            cleanDB();

            try {
                //Get list of modules
                JSONArray module = json[0].getJSONArray(TAG_MODULE);
                //Get list of sensors
                JSONArray sensor = json[0].getJSONArray(TAG_SENSOR);
                //Get list of association between sensors and module
                JSONArray asso = json[0].getJSONArray(TAG_ASSO);

                //Store modules, sensors and association to the database
                storeModule(module);
                storeSensor(sensor);
                storeAssociation(asso);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Clean the database
     */
    protected void cleanDB() {
        //Get all DAO
        ModulesAndSensorsAssociationsDao assoDao=daoFactory.getAssociationDao();
        SensorsDao sensorDao=daoFactory.getSensorDao();
        ModulesDao moduleDao=daoFactory.getModuleDao();
        try {
            //Clean all table
            assoDao.clean();
            sensorDao.clean();
            moduleDao.clean();
        }catch(DaoException e){

        }
        //Close all DAO
        assoDao.close();
        sensorDao.close();
        moduleDao.close();
    }

    /**
     * Store modules to the database
     * @param modules
     */
    protected void storeModule(JSONArray modules){
        //Get the module DAO
        ModulesDao moduleDao=daoFactory.getModuleDao();

        //Foreach module
        for(int i=0;i<modules.length();i++) {
            try {
                JSONObject jsonObj=modules.getJSONObject(i);

                //Create module from the jsonObject
                ModuleArduino module=moduleBuilder.createModuleArduino(jsonObj);

                //Insert the module to the database
                moduleDao.insertModule(module);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (DaoException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Store sensors to the database
     * @param sensors
     */
    protected void storeSensor(JSONArray sensors){
        //Get the sensor DAO
        SensorsDao sensorDao=daoFactory.getSensorDao();

        //Foreach sensors
        for(int i=0;i<sensors.length();i++) {
            try {
                JSONObject jsonObj=sensors.getJSONObject(i);

                //Create sensor from the jsonObject
                Sensor sensor=sensorBuilder.create(jsonObj);

                //Insert the sensor to the database
                sensorDao.insertSensor(sensor);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (DaoException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Store associations to the database
     * @param assos
     */
    protected void storeAssociation(JSONArray assos){
        //Get the association DAO
        ModulesAndSensorsAssociationsDao assoDao=daoFactory.getAssociationDao();

        //Foreach associations
        for(int i=0;i<assos.length();i++) {
            try {
                JSONObject jsonObj=assos.getJSONObject(i);

                //Get module id
                int idModule=jsonObj.getInt("MODULEID");
                //Get sensor id
                int idSensor=jsonObj.getInt("SENSORID");

                //Insert the association to the database
                assoDao.insertAssociation(idModule,idSensor);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (DaoException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Says if the system of module arduino know an address of module
     * @param address Address of the supposed module
     * @return Return true if the module is know, else false
     */
    public boolean knowModule(String address){
        //Get the module DAO
        ModulesDao modulesDao=daoFactory.getModuleDao();
        try {

            //Get module for this address
            ModuleArduino modules=modulesDao.getModule(address);

            return modules!=null;
        } catch (DaoException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Get the list of available sensors
     * @return Return the list of available sensor
     */
    public List<Integer> getAvailableSensor(){
        return availableSensor;
    }

    /**
     * Get a module arduino from an address
     * @param address Address of the module
     * @return Return the module if is know, null else.
     */
    public ModuleArduino getModule(String address) {
        //Get the module DAO
        ModulesDao modulesDao=daoFactory.getModuleDao();

        //Get the sensor DAO
        SensorsDao sensorsDao=daoFactory.getSensorDao();
        try {

            //Get the module correspond to the address
            ModuleArduino modules=modulesDao.getModule(address);

            //Add sensors connect on the module
            if(modules!=null)
                modules.addSensors(sensorsDao.getSensors(modules));

            //Return the module
            return modules;
        } catch (DaoException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void receiveData(Sensor sensor){
        this.setChanged();
        this.notifyObservers(sensor);
    }
}
