package com.arduino.module;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * This class represent a builder for module arduino
 */
public class ModuleArduinoBuilder implements Serializable{

    /**
     * Create a module arduino from a json object
     * @param json Json object represent a module arduino
     * @return Return module arduino build
     */
    public ModuleArduino createModuleArduino(JSONObject json){
        int id=-1;
        String address="";

        //Get module id
        try {
            if(json.has("ID")) {
                id = json.getInt("ID");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //Get module address
        try {
            if(json.has("ADDRESS")) {
                address = json.getString("ADDRESS");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //Return module arduino
        return new ModuleArduino(id,address);
    }

    /**
     * Create a module arduino from its id and address
     * @param id Id of module arduino
     * @param address Address of a module arduino
     * @return Return module arduino build
     */
    public ModuleArduino createModuleArduino(int id,String address){
        return new ModuleArduino(id,address);
    }
}
