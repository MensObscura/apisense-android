package com.arduino.module;

import com.arduino.sensors.Sensor;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represent a Module
 */
public class ModuleArduino {

    // Bluetooth address of the module
    private String address;

    private int id;

    // Sensors associated to the module
    private List<Sensor> listSensor;

    public ModuleArduino(int id,String address){
        this.id=id;
        this.address=address;
        listSensor=new ArrayList<Sensor>();
    }

    /**
     * Get id of the module
     * @return Id of the module
     */
    public int getId(){
        return id;
    }

    /**
     * Get address of the module
     * @return Address of the module
     */
    public String getAddress(){
        return address;
    }

    /**
     * Get sensors connect on the module
     * @return List of sensors connect on the module
     */
    public List<Sensor> getListSensor(){
        return listSensor;
    }

    public boolean equals(Object o){
        if(o.getClass().equals(this.getClass())){
            ModuleArduino module=(ModuleArduino) o;
            return id == ((ModuleArduino) o).getId();
        }
        return false;
    }

    /**
     * Add sensors connect on the module
     * @param sensors
     */
    public void addSensors(List<Sensor> sensors){listSensor.addAll(sensors);}
}
