package com.arduino.module;

import android.util.Log;

import com.RobolectricGradleTestRunner;
import com.arduino.database.dao.DaoException;
import com.arduino.database.dao.DaoFactory;
import com.arduino.database.dao.ModulesAndSensorsAssociationsDao;
import com.arduino.database.dao.ModulesDao;
import com.arduino.database.dao.SensorsDao;
import com.arduino.sensors.Sensor;
import com.arduino.sensors.SensorBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(RobolectricGradleTestRunner.class)
@PrepareForTest({ SystemModuleArduino.class })
public class SystemModuleArduinoTest {

    private DaoFactory daoFact;
    private ModuleArduinoBuilder moduleArduinoBuilder;
    private SensorBuilder sensorBuilder;
    private JSONParser jsonParser;
    private SystemModuleArduino sysModuleArd;
    private ModulesDao moduleDao;
    private SensorsDao sensorDao;
    private ModulesAndSensorsAssociationsDao assoDao;
    private String url;
    private List<Integer> list_sensors;

    @Before
    public void setUp(){
        daoFact= mock(DaoFactory.class);
        moduleArduinoBuilder=mock(ModuleArduinoBuilder.class);
        sensorBuilder=mock(SensorBuilder.class);
        jsonParser=mock(JSONParser.class);
        list_sensors=mock(List.class);

        setMockDao();

        url="www.une_url.fr";
        sysModuleArd=new SystemModuleArduino(url,list_sensors, daoFact, moduleArduinoBuilder, sensorBuilder, jsonParser);
    }

    protected void setMockDao(){
        moduleDao=mock(ModulesDao.class);
        sensorDao=mock(SensorsDao.class);
        assoDao=mock(ModulesAndSensorsAssociationsDao.class);
        when(daoFact.getModuleDao()).thenReturn(moduleDao);
        when(daoFact.getSensorDao()).thenReturn(sensorDao);
        when(daoFact.getAssociationDao()).thenReturn(assoDao);
    }

    @Test
    public void testCleanBDD() throws DaoException {
        sysModuleArd.cleanDB();

        verify(daoFact,times(1)).getModuleDao();
        verify(daoFact,times(1)).getSensorDao();
        verify(daoFact,times(1)).getAssociationDao();
        verify(moduleDao,times(1)).clean();
        verify(sensorDao,times(1)).clean();
        verify(assoDao,times(1)).clean();
        verify(moduleDao,times(1)).close();
        verify(sensorDao,times(1)).close();
        verify(assoDao,times(1)).close();
    }

    @Test
    public void testNoStoreModule() throws DaoException {
        JSONArray jsonArray=new JSONArray();
        sysModuleArd.storeModule(jsonArray);

        verify(daoFact,times(1)).getModuleDao();
        verify(moduleArduinoBuilder,never()).createModuleArduino(null);
        verify(moduleDao,never()).insertModule(null);
    }

    @Test
    public void testStore1Module() throws DaoException, JSONException {
        JSONArray jsonArray=new JSONArray();
        jsonArray.put(new JSONObject());
        ModuleArduino module=mock(ModuleArduino.class);
        when(moduleArduinoBuilder.createModuleArduino(jsonArray.getJSONObject(0))).thenReturn(module);
        when(moduleDao.insertModule(module)).thenReturn(1l);

        sysModuleArd.storeModule(jsonArray);

        verify(daoFact,times(1)).getModuleDao();
        verify(moduleArduinoBuilder,times(1)).createModuleArduino(jsonArray.getJSONObject(0));
        verify(moduleDao,times(1)).insertModule(module);
    }

    @Test
    public void testStore2Module() throws DaoException, JSONException {
        JSONArray jsonArray=new JSONArray();
        jsonArray.put(new JSONObject());
        jsonArray.put(new JSONObject());
        ModuleArduino module1=mock(ModuleArduino.class);
        ModuleArduino module2=mock(ModuleArduino.class);
        when(moduleArduinoBuilder.createModuleArduino(jsonArray.getJSONObject(0))).thenReturn(module1);
        when(moduleArduinoBuilder.createModuleArduino(jsonArray.getJSONObject(1))).thenReturn(module2);
        when(moduleDao.insertModule(module1)).thenReturn(1L);
        when(moduleDao.insertModule(module2)).thenReturn(2L);

        sysModuleArd.storeModule(jsonArray);

        verify(daoFact,times(1)).getModuleDao();
        verify(moduleArduinoBuilder,times(1)).createModuleArduino(jsonArray.getJSONObject(0));
        verify(moduleDao,times(1)).insertModule(module1);
        verify(moduleArduinoBuilder,times(1)).createModuleArduino(jsonArray.getJSONObject(1));
        verify(moduleDao,times(1)).insertModule(module2);
    }

    @Test
    public void testNoStoreSensor() throws DaoException {
        JSONArray jsonArray=new JSONArray();
        sysModuleArd.storeSensor(jsonArray);

        verify(daoFact,times(1)).getSensorDao();
        verify(sensorBuilder,never()).create(null);
        verify(sensorDao,never()).insertSensor(null);
    }

    @Test
    public void testStore1Sensor() throws DaoException, JSONException {
        JSONArray jsonArray=new JSONArray();
        jsonArray.put(new JSONObject());
        Sensor sensor=mock(Sensor.class);
        when(sensorBuilder.create(jsonArray.getJSONObject(0))).thenReturn(sensor);
        when(sensorDao.insertSensor(sensor)).thenReturn(1L);

        sysModuleArd.storeSensor(jsonArray);

        verify(daoFact,times(1)).getSensorDao();
        verify(sensorBuilder,times(1)).create(jsonArray.getJSONObject(0));
        verify(sensorDao,times(1)).insertSensor(sensor);
    }

    @Test
    public void testStore2Sensor() throws DaoException, JSONException {
        JSONArray jsonArray=new JSONArray();
        jsonArray.put(new JSONObject());
        jsonArray.put(new JSONObject());
        Sensor sensor1=mock(Sensor.class);
        Sensor sensor2=mock(Sensor.class);
        when(sensorBuilder.create(jsonArray.getJSONObject(0))).thenReturn(sensor1);
        when(sensorBuilder.create(jsonArray.getJSONObject(1))).thenReturn(sensor2);
        when(sensorDao.insertSensor(sensor1)).thenReturn(1L);
        when(sensorDao.insertSensor(sensor2)).thenReturn(2L);

        sysModuleArd.storeSensor(jsonArray);

        verify(daoFact,times(1)).getSensorDao();
        verify(sensorBuilder,times(1)).create(jsonArray.getJSONObject(0));
        verify(sensorDao,times(1)).insertSensor(sensor1);
        verify(sensorBuilder,times(1)).create(jsonArray.getJSONObject(1));
        verify(sensorDao,times(1)).insertSensor(sensor2);
    }

    @Test
    public void testNoStoreAsso() throws DaoException {
        JSONArray jsonArray=new JSONArray();
        sysModuleArd.storeAssociation(jsonArray);

        verify(daoFact,times(1)).getAssociationDao();
        verify(assoDao,never()).insertAssociation(0,0);
    }

    @Test
    public void testStore1Asso() throws DaoException, JSONException {
        JSONArray jsonArray=new JSONArray();
        JSONObject jsonObj=mock(JSONObject.class);
        when(jsonObj.getInt("MODULEID")).thenReturn(1);
        when(jsonObj.getInt("SENSORID")).thenReturn(1);
        jsonArray.put(jsonObj);

        sysModuleArd.storeAssociation(jsonArray);

        verify(daoFact,times(1)).getAssociationDao();
        verify(jsonObj,times(1)).getInt("MODULEID");
        verify(jsonObj,times(1)).getInt("SENSORID");
        verify(assoDao, times(1)).insertAssociation(1,1);
    }

    @Test
    public void testStore2Asso() throws DaoException, JSONException {
        JSONArray jsonArray=new JSONArray();
        JSONObject jsonObj=mock(JSONObject.class);
        when(jsonObj.getInt("MODULEID")).thenReturn(2);
        when(jsonObj.getInt("SENSORID")).thenReturn(2);
        JSONObject jsonObj2=mock(JSONObject.class);
        when(jsonObj2.getInt("MODULEID")).thenReturn(1);
        when(jsonObj2.getInt("SENSORID")).thenReturn(1);
        jsonArray.put(jsonObj);
        jsonArray.put(jsonObj2);

        sysModuleArd.storeAssociation(jsonArray);

        verify(daoFact,times(1)).getAssociationDao();
        verify(jsonObj,times(1)).getInt("MODULEID");
        verify(jsonObj,times(1)).getInt("SENSORID");
        verify(jsonObj2,times(1)).getInt("MODULEID");
        verify(jsonObj2,times(1)).getInt("SENSORID");
        verify(assoDao,times(1)).insertAssociation(2,2);
        verify(assoDao,times(1)).insertAssociation(1,1);
    }

    @Test
    public void testNoKnowModule() throws DaoException {
        String address="ABCDE";
        when(moduleDao.getModule(address)).thenReturn(null);

        assertFalse(sysModuleArd.knowModule(address));

        verify(daoFact,times(1)).getModuleDao();
        verify(moduleDao,times(1)).getModule(address);
    }

    @Test
    public void testKnowModule() throws DaoException {
        String address="ABCDE";
        ModuleArduino mod=mock(ModuleArduino.class);
        when(moduleDao.getModule(address)).thenReturn(mod);

        assertTrue(sysModuleArd.knowModule(address));

        verify(daoFact,times(1)).getModuleDao();
        verify(moduleDao,times(1)).getModule(address);
    }

    @Test
    public void testGetModuleEmpty() throws DaoException {
        String address="ABCDE";
        when(moduleDao.getModule(address)).thenReturn(null);

        assertNull(sysModuleArd.getModule(address));

        verify(daoFact,times(1)).getModuleDao();
        verify(daoFact,times(1)).getSensorDao();
        verify(moduleDao,times(1)).getModule(address);
        verify(sensorDao,never()).getSensors(null);
    }

    @Test
    public void testGetModule() throws DaoException {
        String address="ABCDE";
        ModuleArduino mod=mock(ModuleArduino.class);
        List<Sensor> list_sensor=new ArrayList<Sensor>();
        doNothing().when(mod).addSensors(list_sensor);
        when(moduleDao.getModule(address)).thenReturn(mod);
        when(sensorDao.getSensors(mod)).thenReturn(list_sensor);

        assertSame(mod, sysModuleArd.getModule(address));

        verify(daoFact,times(1)).getModuleDao();
        verify(daoFact,times(1)).getSensorDao();
        verify(moduleDao,times(1)).getModule(address);
        verify(sensorDao,times(1)).getSensors(mod);
        verify(mod,times(1)).addSensors(list_sensor);
    }
}
