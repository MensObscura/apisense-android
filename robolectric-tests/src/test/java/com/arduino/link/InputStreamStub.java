package com.arduino.link;

import java.io.IOException;
import java.io.InputStream;

public class InputStreamStub extends InputStream {
    private int passage=0;
    private String request;

    public InputStreamStub(String request){
        this.request=request;
    }

    @Override
    public int read() throws IOException {
        return 0;
    }

    public int read(byte[] b) throws IOException{
        if(passage<request.length()+2){
            String req="<"+request+">";
            Character c=req.charAt(passage);
            Integer cVal=(int)c.charValue();
            byte b2[]=new byte[1];
            b2[0]=(cVal.byteValue());
            for(int i=0;i<b2.length;i++){
                b[i]=b2[i];
            }
            passage++;
            return b2.length;
        }
        return -1;
    }
}
