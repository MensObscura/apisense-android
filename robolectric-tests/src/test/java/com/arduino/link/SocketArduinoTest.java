package com.arduino.link;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;

import com.RobolectricGradleTestRunner;
import com.arduino.module.ModuleArduino;
import com.arduino.module.SystemModuleArduino;
import com.arduino.sensors.Sensor;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(RobolectricGradleTestRunner.class)
@PrepareForTest({ SocketArduino.class })
public class SocketArduinoTest {

    protected BufferedWriter bos;
    protected OutputStream os;
    protected InputStream is;
    protected BluetoothDevice device;
    protected BluetoothSocket blueSoc;
    protected SystemModuleArduino sysModArduino;
    protected SocketArduino soc;
    protected ModuleArduino mod;

    @Before
    public void setUp() throws IOException {
        bos=mock(BufferedWriter.class);
        os=mock(OutputStream.class);
        is=mock(InputStream.class);
        device=mock(BluetoothDevice.class);
        sysModArduino=mock(SystemModuleArduino.class);
        blueSoc=mock(BluetoothSocket.class);
        mod=mock(ModuleArduino.class);

        when(sysModArduino.getModule(anyString())).thenReturn(mod);
        doNothing().when(os).close();
        doNothing().when(is).close();
        doNothing().when(blueSoc).close();


        soc=new SocketArduino(device,sysModArduino);
    }

    @Test
    public void testSendRequest() throws IOException {
        soc.bos=bos;
        String request="req";
        doNothing().when(bos).write("<"+request+">");
        doNothing().when(bos).flush();

        soc.sendRequest(request);

        verify(bos,times(1)).write("<"+request+">");
        verify(bos,times(1)).flush();
    }

    @Test
    public void testNoReadData() throws IOException {
        when(is.read(any(byte[].class))).thenReturn(-1);
        soc.is=is;

        assertEquals("", soc.readData());

        verify(is,times(1)).read(any(byte[].class));
    }

    @Test
    public void testReadDataWithNoCommand() throws IOException {
        when(is.read(any(byte[].class))).thenReturn(5).thenReturn(-1);
        soc.is=is;

        assertEquals("", soc.readData());

        verify(is,times(2)).read(any(byte[].class));
    }

    @Test
    public void testReadData() throws IOException {
        InputStreamStub is=new InputStreamStub("request");
        soc.is=is;

        assertEquals("request", soc.readData());
    }

    @Test
    public void testReadData2() throws IOException {
        InputStreamStub is=new InputStreamStub("request><req2");
        soc.is=is;

        assertEquals("request", soc.readData());
        assertEquals("req2", soc.readData());
    }

    @Test
    public void testRunWithNoSensor() throws IOException {
        when(device.createRfcommSocketToServiceRecord(any(UUID.class))).thenReturn(blueSoc);
        doNothing().when(blueSoc).connect();
        when(blueSoc.getOutputStream()).thenReturn(os);
        when(blueSoc.getInputStream()).thenReturn(is);
        when(mod.getListSensor()).thenReturn(new ArrayList<Sensor>());
        when(sysModArduino.getAvailableSensor()).thenReturn(new ArrayList<Integer>());

        soc.run();

        verify(device,times(1)).createRfcommSocketToServiceRecord(any(UUID.class));
        verify(blueSoc,times(1)).connect();
        verify(blueSoc,times(1)).getOutputStream();
        verify(blueSoc,times(1)).getInputStream();
        verify(mod,times(1)).getListSensor();
        verify(os,times(1)).close();
        verify(is,times(1)).close();
        verify(blueSoc,times(1)).close();
        verify(sysModArduino,never()).receiveData(any(Sensor.class));
        verify(sysModArduino,times(1)).getModule(anyString());
        verify(sysModArduino,times(1)).getAvailableSensor();
    }

    @Test
    public void testRunWithNoAvalaibleSensor() throws IOException {
        Sensor sensor=mock(Sensor.class);
        ArrayList<Sensor> list_sensor=new ArrayList<Sensor>();
        ArrayList<Integer> list_avalaible=mock(ArrayList.class);
        list_sensor.add(sensor);
        when(sensor.getId()).thenReturn(1);
        when(device.createRfcommSocketToServiceRecord(any(UUID.class))).thenReturn(blueSoc);
        doNothing().when(blueSoc).connect();
        when(blueSoc.getOutputStream()).thenReturn(os);
        when(blueSoc.getInputStream()).thenReturn(is);
        when(mod.getListSensor()).thenReturn(list_sensor);
        when(sysModArduino.getAvailableSensor()).thenReturn(list_avalaible);
        when(list_avalaible.contains(anyInt())).thenReturn(false);

        soc.run();

        verify(sensor,times(1)).getId();
        verify(device, times(1)).createRfcommSocketToServiceRecord(any(UUID.class));
        verify(blueSoc,times(1)).connect();
        verify(blueSoc,times(1)).getOutputStream();
        verify(blueSoc,times(1)).getInputStream();
        verify(mod,times(1)).getListSensor();
        verify(os,times(1)).close();
        verify(is,times(1)).close();
        verify(blueSoc,times(1)).close();
        verify(sysModArduino,never()).receiveData(any(Sensor.class));
        verify(sysModArduino,times(1)).getModule(anyString());
        verify(sysModArduino,times(1)).getAvailableSensor();
        verify(list_avalaible,times(1)).contains(1);
    }

    @Test
    public void testRunWithAvalaibleSensor() throws IOException {
        Sensor sensor=mock(Sensor.class);
        ArrayList<Sensor> list_sensor=new ArrayList<Sensor>();
        ArrayList<Integer> list_avalaible=mock(ArrayList.class);
        InputStreamStub is=new InputStreamStub("10.5");
        list_sensor.add(sensor);
        when(sensor.getId()).thenReturn(1);
        when(sensor.getName()).thenReturn("LM393");
        when(device.createRfcommSocketToServiceRecord(any(UUID.class))).thenReturn(blueSoc);
        doNothing().when(blueSoc).connect();
        when(blueSoc.getOutputStream()).thenReturn(os);
        when(blueSoc.getInputStream()).thenReturn(is);
        when(mod.getListSensor()).thenReturn(list_sensor);
        when(sysModArduino.getAvailableSensor()).thenReturn(list_avalaible);
        when(list_avalaible.contains(anyInt())).thenReturn(true);

        soc.run();

        verify(sensor,times(1)).getId();
        verify(sensor,times(1)).getName();
        byte[] b=new byte[8192];
        byte[] b2=("<LM393>").getBytes();
        int i=0;
        for(byte btmp:b2){
            b[i++]=btmp;
        }
        verify(os,times(1)).write(b,0,7);
        verify(os,times(1)).flush();
        verify(device,times(1)).createRfcommSocketToServiceRecord(any(UUID.class));
        verify(blueSoc,times(1)).connect();
        verify(blueSoc,times(1)).getOutputStream();
        verify(blueSoc,times(1)).getInputStream();
        verify(mod,times(1)).getListSensor();
        verify(os,times(1)).close();
        verify(blueSoc,times(1)).close();
        verify(sysModArduino,times(1)).receiveData(sensor);
        verify(sysModArduino,times(1)).getModule(anyString());
        verify(sysModArduino,times(1)).getAvailableSensor();
        verify(list_avalaible,times(1)).contains(1);
        verify(sensor,times(1)).setData(10.5);
    }

    @Test
    public void testRunWith2AvalaibleSensor() throws IOException {
        Sensor sensor=mock(Sensor.class);
        Sensor sensor2=mock(Sensor.class);
        ArrayList<Sensor> list_sensor=new ArrayList<Sensor>();
        ArrayList<Integer> list_avalaible=mock(ArrayList.class);
        InputStreamStub is=new InputStreamStub("10.5><9.5");
        list_sensor.add(sensor);
        list_sensor.add(sensor2);
        when(sensor.getId()).thenReturn(1);
        when(sensor.getName()).thenReturn("LM393");
        when(sensor2.getId()).thenReturn(2);
        when(sensor2.getName()).thenReturn("MQ3");
        when(device.createRfcommSocketToServiceRecord(any(UUID.class))).thenReturn(blueSoc);
        doNothing().when(blueSoc).connect();
        when(blueSoc.getOutputStream()).thenReturn(os);
        when(blueSoc.getInputStream()).thenReturn(is);
        when(mod.getListSensor()).thenReturn(list_sensor);
        when(sysModArduino.getAvailableSensor()).thenReturn(list_avalaible);
        when(list_avalaible.contains(anyInt())).thenReturn(true);

        soc.run();

        verify(sensor,times(1)).getId();
        verify(sensor,times(1)).getName();
        verify(sensor2,times(1)).getId();
        verify(sensor2,times(1)).getName();
        verify(os,times(2)).flush();
        verify(device,times(1)).createRfcommSocketToServiceRecord(any(UUID.class));
        verify(blueSoc,times(1)).connect();
        verify(blueSoc,times(1)).getOutputStream();
        verify(blueSoc,times(1)).getInputStream();
        verify(mod,times(1)).getListSensor();
        verify(os,times(1)).close();
        verify(blueSoc,times(1)).close();
        verify(sysModArduino,times(1)).receiveData(sensor);
        verify(sysModArduino,times(1)).getModule(anyString());
        verify(sysModArduino,times(1)).getAvailableSensor();
        verify(list_avalaible,times(1)).contains(1);
        verify(list_avalaible,times(1)).contains(2);
        verify(sensor,times(1)).setData(10.5);
        verify(sensor2,times(1)).setData(9.5);
    }
}
