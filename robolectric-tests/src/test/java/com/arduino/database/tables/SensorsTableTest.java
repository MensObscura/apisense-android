package com.arduino.database.tables;


import android.database.sqlite.SQLiteDatabase;

import com.RobolectricGradleTestRunner;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(RobolectricGradleTestRunner.class)
@PrepareForTest({ SensorsTable.class })
public class SensorsTableTest {
    private SQLiteDatabase db;
    private SensorsTable sensorsTable;

    @Before
    public void setUp(){
        db=mock(SQLiteDatabase.class);
        sensorsTable=new SensorsTable();
    }

    @Test
    public void testOnCreate(){
        String sql="CREATE TABLE table_sensors (ID INTEGER PRIMARY KEY AUTOINCREMENT, NAME TEXT NOT NULL, TYPE TEXT NOT NULL);";
        doNothing().when(db).execSQL(sql);
        sensorsTable.onCreate(db);
        verify(db,times(1)).execSQL(sql);
    }

    @Test
    public void testOnUpdate(){
        String sql="DROP TABLE IF EXISTS table_sensors;";
        doNothing().when(db).execSQL(sql);
        sensorsTable.onUpgrade(db);
        verify(db,times(1)).execSQL(sql);
    }
}
