package com.arduino.database.tables;


import android.database.sqlite.SQLiteDatabase;

import com.RobolectricGradleTestRunner;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(RobolectricGradleTestRunner.class)
@PrepareForTest({ ModulesAndSensorsAssociationsTable.class })

public class ModulesAndSensorsTableTest {
    private SQLiteDatabase db;
    private ModulesAndSensorsAssociationsTable assoTable;

    @Before
    public void setUp(){
        db=mock(SQLiteDatabase.class);
        assoTable=new ModulesAndSensorsAssociationsTable();
    }

    @Test
    public void testOnCreate(){
        String sql="CREATE TABLE table_associations (ID INTEGER PRIMARY KEY AUTOINCREMENT, MODULE_ID INTEGER NOT NULL, SENSOR_ID INTEGER NOT NULL);";
        doNothing().when(db).execSQL(sql);
        assoTable.onCreate(db);
        verify(db,times(1)).execSQL(sql);
    }

    @Test
    public void testOnUpdate(){
        String sql="DROP TABLE IF EXISTS table_associations;";
        doNothing().when(db).execSQL(sql);
        assoTable.onUpgrade(db);
        verify(db,times(1)).execSQL(sql);
    }
}
