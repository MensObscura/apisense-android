package com.arduino.database.tables;

import android.database.sqlite.SQLiteDatabase;

import com.RobolectricGradleTestRunner;
import com.arduino.database.dao.ModulesDao;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(RobolectricGradleTestRunner.class)
@PrepareForTest({ ModulesTable.class })
public class ModulesTableTest {

    private SQLiteDatabase db;
    private ModulesTable modulesTable;

    @Before
    public void setUp(){
        db=mock(SQLiteDatabase.class);
        modulesTable=new ModulesTable();
    }

    @Test
    public void testOnCreate(){
        String sql="CREATE TABLE table_modules (ID INTEGER PRIMARY KEY AUTOINCREMENT, ADDRESS TEXT NOT NULL);";
        doNothing().when(db).execSQL(sql);
        modulesTable.onCreate(db);
        verify(db,times(1)).execSQL(sql);
    }

   @Test
    public void testOnUpdate(){
        String sql="DROP TABLE IF EXISTS table_modules;";
        doNothing().when(db).execSQL(sql);
        modulesTable.onUpgrade(db);
        verify(db,times(1)).execSQL(sql);
    }
}
