package com.arduino.database.dao;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

import com.RobolectricGradleTestRunner;
import com.arduino.database.SQLiteHelper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.internal.matchers.Null;
import org.powermock.core.classloader.annotations.PrepareForTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(RobolectricGradleTestRunner.class)
@PrepareForTest({ ModulesAndSensorsAssociationsDao.class })

public class ModulesAndSensorsAssociationsDaoTest {

    private SQLiteHelper helper;
    private SQLiteDatabase db;
    private ModulesAndSensorsAssociationsDao assoDao;

    @Before
    public void setUp(){
        helper=mock(SQLiteHelper.class);
        db=mock(SQLiteDatabase.class);
        when(helper.getWritableDatabase()).thenReturn(db);
        when(db.isOpen()).thenReturn(true);
        doNothing().when(db).close();
        assoDao=new ModulesAndSensorsAssociationsDao(helper);
    }

    @Test
    public void testOpenDB(){
        when(db.isOpen()).thenReturn(true);
        assoDao.open();

        assertSame(db, assoDao.getDB());
        assertTrue(assoDao.isOpen());

        try {
            verify(helper,times(1)).getWritableDatabase();
            verify(db,times(1)).isOpen();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testCloseDB(){
        when(db.isOpen()).thenReturn(true).thenReturn(false);
        assoDao.open();
        assoDao.close();

        assertSame(db, assoDao.getDB());
        assertFalse(db.isOpen());

        try {
            verify(helper,times(1)).getWritableDatabase();
            verify(db,times(1)).close();
            verify(db,times(2)).isOpen();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testremoveAssociation1() throws DaoException {
        when(db.isOpen()).thenReturn(true);
        when(db.delete("table_associations","ID = 0",null)).thenReturn(0);

        assoDao.open();
        int val=assoDao.removeAssociation(0);

        assertEquals(0,val);

        verify(db,times(1)).delete("table_associations", "ID = 0", null);
        verify(db,times(1)).isOpen();
    }

    @Test
    public void testremoveAssociation2() throws DaoException {
        when(db.isOpen()).thenReturn(true);
        when(db.delete("table_associations","ID = 1",null)).thenReturn(5);

        assoDao.open();
        int val=assoDao.removeAssociation(1);

        assertEquals(5,val);

        verify(db,times(1)).delete("table_associations", "ID = 1", null);
        verify(db,times(1)).isOpen();
    }

    @Test
    public void testclean() throws DaoException {
        when(db.isOpen()).thenReturn(true);
        when(db.delete("table_associations",null,null)).thenReturn(0);

        assoDao.open();
        assoDao.clean();

        verify(db,times(1)).delete("table_associations",null,null);
        verify(db,times(1)).isOpen();
    }

    @Test
    public void testInsertAssociation1() throws DaoException {
        int idModule=1;
        int idSensor=2;

        ContentValues cv=new ContentValues();
        cv.put("MODULE_ID",idModule);
        cv.put("SENSOR_ID",idSensor);

        when(db.isOpen()).thenReturn(true);
        when(db.insert("table_associations",null,cv)).thenReturn(1l);

        assoDao.open();
        long val=assoDao.insertAssociation(idModule,idSensor);

        assertEquals(1l,val);

        verify(db,times(1)).insert("table_associations", null, cv);
        verify(db,times(1)).isOpen();
    }

    @Test
    public void testInsertAssociation2() throws DaoException {
        int idModule=2;
        int idSensor=3;

        ContentValues cv=new ContentValues();
        cv.put("MODULE_ID",idModule);
        cv.put("SENSOR_ID",idSensor);

        when(db.isOpen()).thenReturn(true);
        when(db.insert("table_associations",null,cv)).thenReturn(2l);

        assoDao.open();
        long val=assoDao.insertAssociation(idModule,idSensor);

        assertEquals(2l,val);

        verify(db,times(1)).insert("table_associations", null, cv);
        verify(db,times(1)).isOpen();
    }

    @Test
    public void testCloseDBWithNoOpen(){
       assoDao.close();

        assertNull(assoDao.getDB());
        assertFalse(assoDao.isOpen());

        verify(db,never()).isOpen();
    }

    @Test(expected = DaoException.class)
    public void testremoveAssociationWithoutOpen() throws DaoException {
        assoDao.removeAssociation(0);
    }

    @Test(expected = DaoException.class)
    public void testcleanWithoutOpen() throws DaoException {
        assoDao.clean();
    }

    @Test(expected = DaoException.class)
    public void testInsertAssociationWithoutOpen() throws DaoException {
        int idModule=1;
        int idSensor=2;

        assoDao.insertAssociation(idModule,idSensor);
    }
}
