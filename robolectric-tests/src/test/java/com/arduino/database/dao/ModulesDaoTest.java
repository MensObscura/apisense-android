package com.arduino.database.dao;


import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.RobolectricGradleTestRunner;
import com.arduino.database.SQLiteHelper;
import com.arduino.module.ModuleArduino;
import com.arduino.module.ModuleArduinoBuilder;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(RobolectricGradleTestRunner.class)
@PrepareForTest({ ModulesDao.class })

public class ModulesDaoTest {

    private SQLiteHelper helper;
    private SQLiteDatabase db;
    private ModuleArduinoBuilder arduinoBuilder;
    private ModulesDao modulesDao;

    @Before
    public void setUp(){
        arduinoBuilder=mock(ModuleArduinoBuilder.class);
        helper=mock(SQLiteHelper.class);
        db=mock(SQLiteDatabase.class);
        when(helper.getWritableDatabase()).thenReturn(db);
        doNothing().when(db).close();
        modulesDao=new ModulesDao(helper,arduinoBuilder);
    }

    @Test
    public void testOpenDB(){
        when(db.isOpen()).thenReturn(true);
        modulesDao.open();

        assertTrue(modulesDao.isOpen());
        assertSame(db, modulesDao.getDB());

        try {
            verify(helper,times(1)).getWritableDatabase();
        } catch (Exception e) {
            e.printStackTrace();
        }
        verify(db,times(1)).isOpen();
    }

    @Test
    public void testGetDB(){
        assertNull(modulesDao.getDB());
        assertFalse(modulesDao.isOpen());
        verify(db,never()).isOpen();
    }

    @Test
    public void testCloseDB(){
        when(db.isOpen()).thenReturn(true).thenReturn(false);
        modulesDao.open();
        modulesDao.close();

        assertSame(db, modulesDao.getDB());
        assertFalse(modulesDao.isOpen());

        try {
            verify(helper,times(1)).getWritableDatabase();
            verify(db,times(1)).close();
            verify(db,times(2)).isOpen();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testCloseDBWithNoOpen(){
        modulesDao.close();

        assertNull(modulesDao.getDB());
        assertFalse(modulesDao.isOpen());

        verify(db,never()).isOpen();
    }

    @Test
    public void testremoveModule1() throws DaoException{
        when(db.isOpen()).thenReturn(true);
        when(db.delete("table_modules","ID = 0",null)).thenReturn(0);
        modulesDao.open();
        int val=modulesDao.removeModule(0);

        assertEquals(0,val);

        verify(db,times(1)).delete("table_modules", "ID = 0", null);
        verify(db,times(1)).isOpen();
    }

    @Test
    public void testremoveModule2() throws DaoException{
        when(db.isOpen()).thenReturn(true);
        when(db.delete("table_modules","ID = 1",null)).thenReturn(5);
        modulesDao.open();
        int val=modulesDao.removeModule(1);

        assertEquals(5,val);

        verify(db,times(1)).delete("table_modules", "ID = 1", null);
        verify(db,times(1)).isOpen();
    }

    @Test
    public void testclean() throws DaoException{
        when(db.isOpen()).thenReturn(true);
        when(db.delete("table_modules",null,null)).thenReturn(0);
        modulesDao.open();
        modulesDao.clean();
        verify(db,times(1)).delete("table_modules",null,null);
        verify(db,times(1)).isOpen();
    }

    @Test
    public void testInsertModule1() throws DaoException{
        String address="azerty";
        ContentValues cv = new ContentValues();
        cv.put("ID",1);
        cv.put("ADDRESS", address);

        ModuleArduino arduino=mock(ModuleArduino.class);

        when(db.isOpen()).thenReturn(true);
        when(db.insert("table_modules",null,cv)).thenReturn(1l);
        when(arduino.getId()).thenReturn(1);
        when(arduino.getAddress()).thenReturn(address);

        modulesDao.open();
        long val=modulesDao.insertModule(arduino);

        assertEquals(1l,val);

        verify(db,times(1)).isOpen();
        verify(db,times(1)).insert("table_modules", null, cv);
        verify(arduino,times(1)).getId();
        verify(arduino,times(1)).getAddress();
    }

    @Test
    public void testInsertModule2() throws DaoException{
        String address="azerty2";
        ContentValues cv = new ContentValues();
        cv.put("ID",2);
        cv.put("ADDRESS", address);

        ModuleArduino arduino=mock(ModuleArduino.class);

        when(db.isOpen()).thenReturn(true);
        when(db.insert("table_modules",null,cv)).thenReturn(2l);
        when(arduino.getId()).thenReturn(2);
        when(arduino.getAddress()).thenReturn(address);

        modulesDao.open();
        long val=modulesDao.insertModule(arduino);

        assertEquals(2l,val);

        verify(db,times(1)).isOpen();
        verify(db,times(1)).insert("table_modules", null, cv);
        verify(arduino,times(1)).getId();
        verify(arduino,times(1)).getAddress();
    }

    @Test
    public void testUpdateModule1() throws DaoException{
        String address="azerty";
        ContentValues cv = new ContentValues();
        cv.put("ADDRESS", address);

        ModuleArduino arduino=mock(ModuleArduino.class);

        when(db.isOpen()).thenReturn(true);
        when(db.update("table_modules", cv, "ID = 1", null)).thenReturn(1);
        when(arduino.getId()).thenReturn(1);
        when(arduino.getAddress()).thenReturn(address);

        modulesDao.open();
        int val=modulesDao.updateModule(arduino);

        assertEquals(1,val);

        verify(db,times(1)).update("table_modules", cv, "ID = 1", null);
        verify(arduino,times(1)).getId();
        verify(arduino,times(1)).getAddress();
        verify(db,times(1)).isOpen();
    }

    @Test
    public void testUpdateModule2() throws DaoException{
        String address="azerty2";
        ContentValues cv = new ContentValues();
        cv.put("ADDRESS", address);

        ModuleArduino arduino=mock(ModuleArduino.class);

        when(db.isOpen()).thenReturn(true);
        when(db.update("table_modules", cv, "ID = 2", null)).thenReturn(2);
        when(arduino.getId()).thenReturn(2);
        when(arduino.getAddress()).thenReturn(address);

        modulesDao.open();
        int val=modulesDao.updateModule(arduino);

        assertEquals(2,val);

        verify(db,times(1)).update("table_modules", cv, "ID = 2", null);
        verify(arduino,times(1)).getId();
        verify(arduino,times(1)).getAddress();
        verify(db,times(1)).isOpen();
    }

    @Test
    public void testGetModuleEmpty() throws DaoException{
        String address="azerty";
        Cursor cursor=mock(Cursor.class);
        String[] colonne={"ID","ADDRESS"};

        when(db.isOpen()).thenReturn(true);
        when(cursor.getCount()).thenReturn(0);
        when(db.query("table_modules", colonne, "ADDRESS LIKE \"" + address + "\"", null, null, null, null)).thenReturn(cursor);

        modulesDao.open();
        ModuleArduino val=modulesDao.getModule(address);

        assertNull(val);

        verify(db,times(1)).query("table_modules", colonne, "ADDRESS LIKE \"" + address + "\"", null, null, null, null);
        verify(cursor,times(1)).getCount();
        verify(db,times(1)).isOpen();
    }

    @Test
    public void testGetModule() throws DaoException{
        String address="azerty";
        ModuleArduino arduino=mock(ModuleArduino.class);
        Cursor cursor=mock(Cursor.class);
        String[] colonne={"ID","ADDRESS"};

        when(db.isOpen()).thenReturn(true);
        when(cursor.getCount()).thenReturn(1);
        when(cursor.getInt(0)).thenReturn(1);
        when(cursor.getString(1)).thenReturn(address);
        when(arduinoBuilder.createModuleArduino(1,address)).thenReturn(arduino);
        when(db.query("table_modules", colonne, "ADDRESS LIKE \"" + address + "\"", null, null, null, null)).thenReturn(cursor);

        modulesDao.open();
        ModuleArduino val=modulesDao.getModule(address);

        assertSame(arduino, val);

        verify(db,times(1)).query("table_modules", colonne, "ADDRESS LIKE \"" + address + "\"", null, null, null, null);
        verify(cursor,times(1)).getCount();
        verify(cursor,times(1)).getInt(0);
        verify(cursor,times(1)).getString(1);
        verify(arduinoBuilder,times(1)).createModuleArduino(1,address);
        verify(db,times(1)).isOpen();
    }

    @Test(expected = DaoException.class)
    public void testRemoveModuleNotOpenDB() throws DaoException{
        modulesDao.removeModule(0);
    }

    @Test(expected = DaoException.class)
    public void testcleanNotOpenDB() throws DaoException{
        modulesDao.clean();
    }

    @Test(expected = DaoException.class)
    public void testInsertModuleNotOpenDB() throws DaoException{
        ModuleArduino arduino=mock(ModuleArduino.class);

        modulesDao.insertModule(arduino);
    }

    @Test(expected = DaoException.class)
    public void testUpdateModuleNotOpenDB() throws DaoException{
        ModuleArduino arduino=mock(ModuleArduino.class);

        modulesDao.updateModule(arduino);
    }

    @Test(expected = DaoException.class)
    public void testGetModuleNotOpenDB() throws DaoException{
        String address="azerty";

        modulesDao.getModule(address);
    }
}