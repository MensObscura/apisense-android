package com.arduino.database.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.RobolectricGradleTestRunner;
import com.arduino.database.SQLiteHelper;
import com.arduino.module.ModuleArduino;
import com.arduino.sensors.Sensor;
import com.arduino.sensors.SensorBuilder;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(RobolectricGradleTestRunner.class)
@PrepareForTest({ SensorsDao.class })

public class SensorsDaoTest {

    private SQLiteHelper helper;
    private SQLiteDatabase db;
    private SensorBuilder sensorBuilder;
    private SensorsDao sensorsDao;

    @Before
    public void setUp(){
        sensorBuilder=mock(SensorBuilder.class);
        helper=mock(SQLiteHelper.class);
        db=mock(SQLiteDatabase.class);
        when(helper.getWritableDatabase()).thenReturn(db);
        doNothing().when(db).close();
        sensorsDao=new SensorsDao(helper,sensorBuilder);
    }

    @Test
    public void testOpenDB(){
        when(db.isOpen()).thenReturn(true);
        sensorsDao.open();

        assertSame(db, sensorsDao.getDB());
        assertTrue(sensorsDao.isOpen());

        try {
            verify(helper,times(1)).getWritableDatabase();
            verify(db,times(1)).isOpen();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testCloseDB(){
        when(db.isOpen()).thenReturn(true).thenReturn(false);
        sensorsDao.open();
        sensorsDao.close();

        assertSame(db, sensorsDao.getDB());
        assertFalse(db.isOpen());

        try {
            verify(helper,times(1)).getWritableDatabase();
            verify(db,times(1)).close();
            verify(db,times(2)).isOpen();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testremoveSensor1() throws DaoException {
        when(db.isOpen()).thenReturn(true);
        when(db.delete("table_sensors","ID = 0",null)).thenReturn(0);
        sensorsDao.open();
        int val=sensorsDao.removeSensor(0);

        assertEquals(0,val);

        verify(db,times(1)).delete("table_sensors", "ID = 0", null);
        verify(db,times(1)).isOpen();
    }

    @Test
    public void testremoveSensor2() throws DaoException {
        when(db.isOpen()).thenReturn(true);
        when(db.delete("table_sensors","ID = 1",null)).thenReturn(5);
        sensorsDao.open();
        int val=sensorsDao.removeSensor(1);

        assertEquals(5,val);

        verify(db,times(1)).delete("table_sensors", "ID = 1", null);
        verify(db,times(1)).isOpen();
    }

    @Test
    public void testclean() throws DaoException {
        when(db.isOpen()).thenReturn(true);
        when(db.delete("table_sensors",null,null)).thenReturn(0);
        sensorsDao.open();
        sensorsDao.clean();

        verify(db,times(1)).delete("table_sensors",null,null);
        verify(db,times(1)).isOpen();
    }

    @Test
    public void testInsertSensor1() throws DaoException {
        String name="azerty";
        String type="type";
        ContentValues cv = new ContentValues();
        cv.put("ID",1);
        cv.put("NAME", name);
        cv.put("TYPE", type);

        Sensor sensor=mock(Sensor.class);

        when(db.insert("table_sensors",null,cv)).thenReturn(1l);
        when(sensor.getId()).thenReturn(1);
        when(sensor.getName()).thenReturn(name);
        when(sensor.getType()).thenReturn(type);
        when(db.isOpen()).thenReturn(true);

        sensorsDao.open();
        long val=sensorsDao.insertSensor(sensor);

        assertEquals(1l,val);

        verify(db,times(1)).isOpen();
        verify(db,times(1)).insert("table_sensors", null, cv);
        verify(sensor,times(1)).getId();
        verify(sensor,times(1)).getName();
        verify(sensor,times(1)).getType();
    }

    @Test
    public void testInsertSensor2() throws DaoException {
        String name="azerty2";
        String type="type2";
        ContentValues cv = new ContentValues();
        cv.put("ID",2);
        cv.put("NAME", name);
        cv.put("TYPE", type);

        Sensor sensor=mock(Sensor.class);

        when(db.insert("table_sensors",null,cv)).thenReturn(2l);
        when(sensor.getId()).thenReturn(2);
        when(sensor.getName()).thenReturn(name);
        when(sensor.getType()).thenReturn(type);
        when(db.isOpen()).thenReturn(true);

        sensorsDao.open();
        long val=sensorsDao.insertSensor(sensor);

        assertEquals(2l,val);

        verify(db,times(1)).isOpen();
        verify(db,times(1)).insert("table_sensors", null, cv);
        verify(sensor,times(1)).getId();
        verify(sensor,times(1)).getName();
        verify(sensor,times(1)).getType();
    }

    @Test
    public void testUpdateSensor1() throws DaoException {
        String name="azerty";
        String type="type";
        ContentValues cv = new ContentValues();
        cv.put("NAME", name);
        cv.put("TYPE",type);

        Sensor sensor=mock(Sensor.class);

        when(db.update("table_sensors",cv,"ID = 1",null)).thenReturn(1);
        when(sensor.getId()).thenReturn(1);
        when(sensor.getName()).thenReturn(name);
        when(sensor.getType()).thenReturn(type);
        when(db.isOpen()).thenReturn(true);

        sensorsDao.open();
        int val=sensorsDao.updateSensor(sensor);

        assertEquals(1,val);

        verify(db,times(1)).isOpen();
        verify(db,times(1)).update("table_sensors",cv,"ID = 1",null);
        verify(sensor,times(1)).getId();
        verify(sensor,times(1)).getName();
        verify(sensor,times(1)).getType();
    }

    @Test
    public void testUpdateSensor2() throws DaoException {
        String name="azerty2";
        String type="type2";
        ContentValues cv = new ContentValues();
        cv.put("NAME", name);
        cv.put("TYPE",type);

        Sensor sensor=mock(Sensor.class);

        when(db.update("table_sensors",cv,"ID = 2",null)).thenReturn(2);
        when(sensor.getId()).thenReturn(2);
        when(sensor.getName()).thenReturn(name);
        when(sensor.getType()).thenReturn(type);
        when(db.isOpen()).thenReturn(true);

        sensorsDao.open();
        int val=sensorsDao.updateSensor(sensor);


        assertEquals(2,val);

        verify(db,times(1)).isOpen();
        verify(db,times(1)).update("table_sensors",cv,"ID = 2",null);
        verify(sensor,times(1)).getId();
        verify(sensor,times(1)).getName();
        verify(sensor,times(1)).getType();
    }

    @Test
    public void testGetSensorEmpty() throws DaoException{
        String name="azerty";
        Cursor cursor=mock(Cursor.class);
        String[] colonne={"ID","NAME","TYPE"};

        when(db.isOpen()).thenReturn(true);
        when(cursor.getCount()).thenReturn(0);
        when(db.query("table_sensors", colonne, "NAME LIKE \"" + name + "\"", null, null, null, null)).thenReturn(cursor);

        sensorsDao.open();
        Sensor val=sensorsDao.getSensor(name);

        assertNull(val);

        verify(db,times(1)).query("table_sensors", colonne, "NAME LIKE \"" + name + "\"", null, null, null, null);
        verify(cursor,times(1)).getCount();
        verify(db,times(1)).isOpen();
    }

    @Test
    public void testGetModule() throws DaoException{
        String name="azerty";
        String type="Alcohol";
        Sensor sensor=mock(Sensor.class);
        Cursor cursor=mock(Cursor.class);
        String[] colonne={"ID","NAME","TYPE"};

        when(db.isOpen()).thenReturn(true);
        when(cursor.getCount()).thenReturn(1);
        when(cursor.getInt(0)).thenReturn(1);
        when(cursor.getString(1)).thenReturn(name);
        when(cursor.getString(2)).thenReturn(type);
        when(sensorBuilder.create(1,name,type)).thenReturn(sensor);
        when(db.query("table_sensors", colonne, "NAME LIKE \"" + name + "\"", null, null, null, null)).thenReturn(cursor);

        sensorsDao.open();
        Sensor val=sensorsDao.getSensor(name);

        assertSame(sensor, val);

        verify(db,times(1)).query("table_sensors", colonne, "NAME LIKE \"" + name + "\"", null, null, null, null);
        verify(cursor,times(1)).getCount();
        verify(cursor,times(1)).getInt(0);
        verify(cursor,times(1)).getString(1);
        verify(cursor,times(1)).getString(2);
        verify(sensorBuilder,times(1)).create(1,name,type);
        verify(db,times(1)).isOpen();
    }

    @Test(expected = DaoException.class)
    public void testRemoveSensorNotOpenDB() throws DaoException{
        sensorsDao.removeSensor(0);
    }

    @Test(expected = DaoException.class)
    public void testcleanNotOpenDB() throws DaoException{
        sensorsDao.clean();
    }

    @Test(expected = DaoException.class)
    public void testInsertSensorNotOpenDB() throws DaoException{
        Sensor sensor=mock(Sensor.class);

        sensorsDao.insertSensor(sensor);
    }

    @Test(expected = DaoException.class)
    public void testUpdateSensorNotOpenDB() throws DaoException{
        Sensor sensor=mock(Sensor.class);

        sensorsDao.updateSensor(sensor);
    }

    @Test(expected = DaoException.class)
    public void testGetSensorNotOpenDB() throws DaoException{
        String name="azerty";

        sensorsDao.getSensor(name);
    }

    @Test
    public void testCloseDBWithNoOpen(){
        sensorsDao.close();

        assertNull(sensorsDao.getDB());
        assertFalse(sensorsDao.isOpen());

        verify(db,never()).isOpen();
    }
}
