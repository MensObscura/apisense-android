package com.arduino.database;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.test.mock.MockContext;
import android.test.mock.MockCursor;

import com.RobolectricGradleTestRunner;
import com.arduino.database.tables.ModulesAndSensorsAssociationsTable;
import com.arduino.database.tables.ModulesTable;
import com.arduino.database.tables.SensorsTable;
import com.arduino.database.tables.TableFactory;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;

import javax.naming.Context;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(RobolectricGradleTestRunner.class)
@PrepareForTest({ SQLiteHelper.class })
public class SQLiteHelperTest {
    private SQLiteHelper sqLiteHelper;
    private TableFactory tableFactory;
    private ModulesAndSensorsAssociationsTable msat;
    private ModulesTable modulesTable;
    private SensorsTable sensorsTable;
    private SQLiteDatabase sqLiteDatabase;

    @Before
    public void setUp(){
        tableFactory=mock(TableFactory.class);
        msat=mock(ModulesAndSensorsAssociationsTable.class);
        modulesTable=mock(ModulesTable.class);
        sensorsTable=mock(SensorsTable.class);
        sqLiteDatabase=mock(SQLiteDatabase.class);
        SQLiteDatabase.CursorFactory cf=mock(SQLiteDatabase.CursorFactory.class);

        when(tableFactory.getAssociationTable()).thenReturn(msat);
        when(tableFactory.getModuleTable()).thenReturn(modulesTable);
        when(tableFactory.getSensorTable()).thenReturn(sensorsTable);

        sqLiteHelper=new SQLiteHelper(mock(android.content.Context.class),"arduinoSDK.db",cf,1,tableFactory);
    }

    @Test
    public void testOnCreate(){
        doNothing().when(msat).onCreate(sqLiteDatabase);
        doNothing().when(modulesTable).onCreate(sqLiteDatabase);
        doNothing().when(sensorsTable).onCreate(sqLiteDatabase);

        sqLiteHelper.onCreate(sqLiteDatabase);

        verify(msat,times(1)).onCreate(sqLiteDatabase);
        verify(modulesTable,times(1)).onCreate(sqLiteDatabase);
        verify(sensorsTable,times(1)).onCreate(sqLiteDatabase);
    }

    @Test
    public void testOnUpdate(){
        doNothing().when(msat).onUpgrade(sqLiteDatabase);
        doNothing().when(modulesTable).onUpgrade(sqLiteDatabase);
        doNothing().when(sensorsTable).onUpgrade(sqLiteDatabase);
        doNothing().when(msat).onCreate(sqLiteDatabase);
        doNothing().when(modulesTable).onCreate(sqLiteDatabase);
        doNothing().when(sensorsTable).onCreate(sqLiteDatabase);

        sqLiteHelper.onUpgrade(sqLiteDatabase,1,2);

        verify(msat,times(1)).onUpgrade(sqLiteDatabase);
        verify(modulesTable,times(1)).onUpgrade(sqLiteDatabase);
        verify(sensorsTable,times(1)).onUpgrade(sqLiteDatabase);
        verify(msat,times(1)).onCreate(sqLiteDatabase);
        verify(modulesTable,times(1)).onCreate(sqLiteDatabase);
        verify(sensorsTable,times(1)).onCreate(sqLiteDatabase);
    }
}
